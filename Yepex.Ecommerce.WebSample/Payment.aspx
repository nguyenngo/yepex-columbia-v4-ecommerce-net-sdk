﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Payment.aspx.cs" Inherits="Yepex.Ecommerce.WebSample.Payment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="page-header">
        Payment Service</h1>
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Payment Service</div>
        </div>
        <div class="panel-body">
            <form accept-charset="UTF-8" runat="server" class="simple_form new_user_basic"
            id="paymentService" method="post" novalidate="novalidate">
            <asp:Panel id="lblErrorPanel" runat="server" Visible="false">
                <div class="alert alert-danger">
                    <asp:Label runat="server"  ID="lblError" /></div>
            </asp:Panel>
            <asp:Panel id="lblSuccessPanel" runat="server" Visible="false">
                <div class="alert alert-success">
                    <asp:Label runat="server"  ID="lblSuccess" /></div>
            </asp:Panel>
            <div class="form-group required">
                <label class="required control-label">
                    Id Transaccion</label><input class="string required form-control" id="IdTransaccion"
                        name="IdTransaccion" placeholder="Enter Id Transaccion" type="text" value="1234567890">
                <p class="help-block">
                    An automatic number that identify the transaction, generate by 3rd parties.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Id App</label><input class="string required form-control" id="IdApp" name="IdApp"
                        placeholder="Enter Id App" type="text" value="1">
                <p class="help-block">
                    The Application ID that store the credit card..</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Alias_Tarjeta</label><input class="string required form-control" id="Alias_Tarjeta"
                        name="Alias_Tarjeta" placeholder="Enter Alias_Tarjeta" type="text" value="AHO1">
                <p class="help-block">
                    Alias given by Client</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Valor_Compra</label><input class="string required form-control" id="Valor_Compra"
                        name="Valor_Compra" placeholder="Enter Valor_Compra" type="text" value="100">
                <p class="help-block">
                    Value of the purchase.</p>
            </div>           
            <div class="form-group required">
                <label class="required control-label">
                    Descripcion_Pago</label><input class="string required form-control" id="Descripcion_Pago"
                        name="Descripcion_Pago" placeholder="Enter Descripcion_Pago" type="text" value="test_Descripcion_Pago">
                <p class="help-block">
                    Description of the content purchase.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Referencia_Pago</label><input class="string required form-control" id="Referencia_Pago"
                        name="Referencia_Pago" placeholder="Enter Referencia_Pago" type="text" value="test_Referencia_Pago">
                <p class="help-block">
                    External reference used on receipt.</p>
            </div>
             <div class="form-group required">
                <label class="required control-label">
                    Cuotas</label><input class="string required form-control" id="Cuotas"
                        name="Cuotas" placeholder="Enter Cuotas" type="text" value="10">
                 <p class="help-block">
                    The Cuotas of payment.</p>
             </div>
            <div class="form-group required">
                <label class="required control-label">
                    IVA</label><input class="string required form-control" id="IVA"
                        name="IVA" placeholder="Enter IVA" type="text" value="IVA">
                 <p class="help-block">
                    The IVA of payment.</p>
            </div>
             <div class="form-group required">
                <label class="required control-label">
                    BaseDevolucionIVA</label><input class="string required form-control" id="baseDevolucionIVA"
                        name="baseDevolucionIVA" placeholder="Enter baseDevolucionIVA" type="text" value="baseDevolucionIVA">
                 <p class="help-block">
                    The baseDevolucionIVA of payment.</p>
            </div>
             <div class="form-group required">
                <label class="required control-label">
                    Propina</label><input class="string required form-control" id="propina"
                        name="propina" placeholder="Enter propina" type="text" value="propina">
                 <p class="help-block">
                    The Propina of payment.</p>
            </div>
             <div class="form-group required">
                <label class="required control-label">
                    IAC</label><input class="string required form-control" id="IAC"
                        name="IAC" placeholder="Enter IAC" type="text" value="IAC">
                 <p class="help-block">
                    The IAC of payment.</p>
            </div>
             <div class="form-group required">
                <label class="required control-label">
                    TasaAeroportuaria</label><input class="string required form-control" id="tasaAeroportuaria"
                        name="tasaAeroportuaria" placeholder="Enter tasaAeroportuaria" type="text" value="tasaAeroportuaria">
                 <p class="help-block">
                    The tasaAeroportuaria of payment.</p>
            </div>
            <div class="form-group">
                <label class="control-label">
                    Informacion_Adicional</label><input class="string form-control" id="Informacion_Adicional"
                        name="Informacion_Adicional" placeholder="Enter Informacion_Adicional" type="text">
                <p class="help-block">
                    Additional Information.</p>
            </div>
            <asp:Button runat="server" ID="PaymentButton" class="btn btn-default" name="commit"
                type="submit" value="Payment" Text="Payment" 
                onclick="PaymentButton_Click" />
            </form>
        </div>
    </div>
</asp:Content>
