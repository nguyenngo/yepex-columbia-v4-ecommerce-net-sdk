﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="GetCard.aspx.cs" Inherits="Yepex.Ecommerce.WebSample.GetCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="page-header">
        Get Encrypted Card Service</h1>
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Get Encrypted Card Service</div>
        </div>
        <div class="panel-body">
            <form accept-charset="UTF-8" runat="server" class="simple_form new_user_basic"
            id="getEncryptedCardService" method="post" novalidate="novalidate">
            <asp:Panel id="lblErrorPanel" runat="server" Visible="false">
                <div class="alert alert-danger">
                    <asp:Label runat="server"  ID="lblError" /></div>
            </asp:Panel>
            <asp:Panel id="lblSuccessPanel" runat="server" Visible="false">
                <div class="alert alert-success">
                    <asp:Label runat="server"  ID="lblSuccess" /></div>
            </asp:Panel>
            <div class="form-group required">
                <label class="required control-label">
                    Id Transaccion</label><input class="string required form-control" id="IdTransaccion"
                        name="IdTransaccion" placeholder="Enter Id Transaccion" type="text" value="1234567890">
                <p class="help-block">
                    An automatic number that identify the transaction, generate by 3rd parties.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Id App</label><input class="string required form-control" id="IdApp" name="IdApp"
                        placeholder="Enter Id App" type="text" value="1">
                <p class="help-block">
                    The Application ID that store the credit card..</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Identificacion_Tarjetahabiente</label><input class="string required form-control"
                        id="Identificacion_Tarjetahabiente" name="Identificacion_Tarjetahabiente" placeholder="Enter Identificacion_Tarjetahabiente"
                        type="text" value="test_id">
                <p class="help-block">
                    ID of the card owner.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Alias_Tarjeta</label><input class="string required form-control" id="Alias_Tarjeta"
                        name="Alias_Tarjeta" placeholder="Enter Alias_Tarjeta" type="text" value="AHO1">
                <p class="help-block">
                    Alias given by Client</p>
            </div>
            <div class="form-group">
                <label class="control-label">
                    Informacion_Adicional</label><input class="string form-control" id="Informacion_Adicional"
                        name="Informacion_Adicional" placeholder="Enter Informacion_Adicional" type="text">
                <p class="help-block">
                    Additional Information.</p>
            </div>
             <asp:Button runat="server" ID="GetCardButton" class="btn btn-default" name="commit"
                type="submit" value="Get Card" Text="Get Card" 
                onclick="GetCardButton_Click" />
            </form>
        </div>
    </div>
</asp:Content>
