﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yepex.Ecommerce.SDK.YepexEcommerceService;
using Yepex.Ecommerce.SDK;

namespace Yepex.Ecommerce.WebSample
{
    public partial class GetCardAlias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblErrorPanel.Visible = false;
            lblSuccessPanel.Visible = false;
        }

        protected void GetCardAliasButton_Click(object sender, EventArgs e)
        {
            try
            {
                String IdTransaccion = Request.Form["IdTransaccion"];
                String IdApp = Request.Form["IdApp"];
                String IdentificacionTarjetahabiente = Request.Form["Identificacion_Tarjetahabiente"];
                solicitudObtenerAlias getCardAliasRequest = new solicitudObtenerAlias();
                getCardAliasRequest.id_Transaccion = IdTransaccion;
                getCardAliasRequest.id_App = IdApp;
                getCardAliasRequest.identificacion_Tarjetahabiente = IdentificacionTarjetahabiente;


                ServiceContext serviceContext = new ServiceContext();
                respuestaObtenerAlias getCardAliasResponse = EcommerceCardService.doGetCardAliasRequest(serviceContext, getCardAliasRequest);
                lblSuccessPanel.Visible = true;
                lblSuccess.Text = "Web service had been called successful !!!<br>";
                lblSuccess.Text += "Server responses IdApp: " + getCardAliasResponse.id_App + "<br>";
                lblSuccess.Text += "Server responses IdTransaccion: " + getCardAliasResponse.id_Transaccion + "<br>";
                lblSuccess.Text += "Server responses Identificacion_Tarjetahabiente: " + getCardAliasResponse.identificacion_Tarjetahabiente + "<br>";
                lblSuccess.Text += "Server responses Alias_Tarjeta: " + getCardAliasResponse.alias_Tarjeta + "<br>";
                lblSuccess.Text += "Server responses Estado: " + getCardAliasResponse.estado + "<br>";

            }
            catch (Exception ex)
            {
                lblErrorPanel.Visible = true;
                lblError.Text = "Cannot send message because : " + ex.Message;
            }
        }
    }
}