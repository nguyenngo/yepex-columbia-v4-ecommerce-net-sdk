﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yepex.Ecommerce.SDK.YepexEcommerceService;
using Yepex.Ecommerce.SDK;
using System.Configuration;
using System.Collections.Specialized;
using System.Web.Configuration;
using System.Xml;
using Yepex.Ecommerce.SDK.Crypto;

namespace Yepex.Ecommerce.WebSample
{
    public partial class Settings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblErrorPanel.Visible = false;
                lblGenerateSuccessPanel.Visible = false;
                lblUpdateSuccessPanel.Visible = false;

                if (ConfigurationManager.GetSection("yellowpepper") != null)
                {
                    NameValueCollection ypConfigSection = ConfigurationManager.GetSection("yellowpepper") as NameValueCollection;
                    if (ypConfigSection[BaseConstants.SettingEcommerceURL] != null)
                    {
                        txtWebServiceURL.Text = ypConfigSection[BaseConstants.SettingEcommerceURL];
                    }
                    if (ypConfigSection[BaseConstants.SettingRSAKeyEncrypted] != null)
                    {
                        txtRSAEncrypted.Text = ypConfigSection[BaseConstants.SettingRSAKeyEncrypted];
                    }
                    if (ypConfigSection[BaseConstants.SettingRSAInitCode] != null)
                    {
                        txtRSAInitCode.Text = ypConfigSection[BaseConstants.SettingRSAInitCode];
                    }
                }
            }
        }

        protected void UpdateButton_Click(object sender, EventArgs e)
        {
            updateYellowPepperSetting(BaseConstants.SettingEcommerceURL, txtWebServiceURL.Text);
            updateYellowPepperSetting(BaseConstants.SettingRSAKeyEncrypted, txtRSAEncrypted.Text);
            updateYellowPepperSetting(BaseConstants.SettingRSAInitCode, txtRSAInitCode.Text);
            lblUpdateSuccessPanel.Visible = true;

        }

        protected void GenerateUpdateButton_Click(object sender, EventArgs e)
        {
            lblGenerateSuccessPanel.Visible = true;
            String rsaInitCode = txtRSAInitCodeGenerate.Text;
            String rsaPlainKey = txtRSAPlainKeyGenerate.Text;
            AESProvider aes = new AESProvider();
            aes.SetKey(rsaInitCode);
            String encryptedKey = aes.Encrypt(rsaPlainKey);
            updateYellowPepperSetting(BaseConstants.SettingRSAKeyEncrypted, encryptedKey);
            updateYellowPepperSetting(BaseConstants.SettingRSAInitCode, rsaInitCode);
            txtRSAEncrypted.Text = encryptedKey;
            txtRSAInitCode.Text = rsaInitCode;
        }

        protected void updateYellowPepperSetting(String name, String newValue)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

            xmlDoc.SelectSingleNode("//yellowpepper/add[@key='" + name + "']").Attributes["value"].Value = newValue;
            xmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

            ConfigurationManager.RefreshSection("yellowpepper");
        }
    }
}