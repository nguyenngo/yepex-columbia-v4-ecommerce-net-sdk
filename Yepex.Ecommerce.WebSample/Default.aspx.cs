﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yepex.Ecommerce.SDK.YepexEcommerceService;
using Yepex.Ecommerce.SDK;

namespace Yepex.Ecommerce.WebSample
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            lblErrorPanel.Visible = false;
            lblSuccessPanel.Visible = false;

        }

        protected void AddCardButton_Click(object sender, EventArgs e)
        {
            try
            {
                String IdTransaccion = Request.Form["IdTransaccion"];
                String IdApp = Request.Form["IdApp"];
                String NumeroTarjeta = Request.Form["Numero_Tarjeta"];
                String Cvv = Request.Form["CVV"];
                String FechaExpiracion = Request.Form["Fecha_Expiracion"];
                String NombreTarjetahabiente = Request.Form["Nombre_Tarjetahabiente"];
                String IdentificacionTarjetahabiente = Request.Form["Identificacion_Tarjetahabiente"];
                String AliasTarjeta = Request.Form["Alias_Tarjeta"];
                String DirreccionCorrespondencia = Request.Form["Dirreccion_Correspondencia"];
                String NumeroCelular = Request.Form["Numero_Celular"];
                String Marca = Request.Form["Marca"];

                solicitudAgregarTarjeta addCardRequest = new solicitudAgregarTarjeta();
                addCardRequest.id_Transaccion = IdTransaccion;
                addCardRequest.id_App = IdApp;
                addCardRequest.numero_Tarjeta = NumeroTarjeta;
                addCardRequest.cvv = Cvv;
                addCardRequest.fecha_Expiracion = FechaExpiracion;
                addCardRequest.nombre_Tarjetahabiente = NombreTarjetahabiente;
                addCardRequest.identificacion_Tarjetahabiente = IdentificacionTarjetahabiente;
                addCardRequest.alias_Tarjeta = AliasTarjeta;
                addCardRequest.dirreccion_Correspondencia = DirreccionCorrespondencia;
                addCardRequest.numero_Celular = NumeroCelular;
                addCardRequest.marca = Marca;

                ServiceContext serviceContext = new ServiceContext();
                respuestaAgregarTarjeta addCardResponse = EcommerceCardService.doAddCardRequest(serviceContext, addCardRequest);
                lblSuccessPanel.Visible = true;
                lblSuccess.Text = "Web service had been called successful !!!<br>";
                lblSuccess.Text += "Server responses IdTransaccion: " + addCardResponse.id_Transaccion + "<br>";
                lblSuccess.Text += "Server responses Estado: " + addCardResponse.estado + "<br>";
                lblSuccess.Text += "Server responses FechaTransaccion: " + addCardResponse.fecha_Transaccion + "<br>";
                lblSuccess.Text += "Server responses CodigoTransaccion: " + addCardResponse.codigo_Transaccion + "<br>";
            }
            catch (Exception ex)
            {
                lblErrorPanel.Visible = true;
                lblError.Text = "Cannot send message because : " + ex.Message;
            }

        }
    }
}
