﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="RemoveCard.aspx.cs" Inherits="Yepex.Ecommerce.WebSample.RemoveCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="page-header">
        Remove Card Service</h1>
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Form Remove Card Service</div>
        </div>
        <div class="panel-body">
            <form accept-charset="UTF-8" runat="server" class="simple_form new_user_basic"
            id="removeCardService" method="post" novalidate="novalidate">
            <asp:Panel id="lblErrorPanel" runat="server" Visible="false">
                <div class="alert alert-danger">
                    <asp:Label runat="server"  ID="lblError" /></div>
            </asp:Panel>
            <asp:Panel id="lblSuccessPanel" runat="server" Visible="false">
                <div class="alert alert-success">
                    <asp:Label runat="server"  ID="lblSuccess" /></div>
            </asp:Panel>
            <div class="form-group required">
                <label class="required control-label">
                    Id Transaccion</label><input class="string required form-control" id="IdTransaccion"
                        name="IdTransaccion" placeholder="Enter Id Transaccion" type="text" value="1234567890">
                <p class="help-block">
                    An automatic number that identify the transaction, generate by 3rd parties.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Id App</label><input class="string required form-control" id="IdApp" name="IdApp"
                        placeholder="Enter Id App" type="text" value="1">
                <p class="help-block">
                    The Application ID that store the credit card..</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Identificacion_Tarjetahabiente</label><input class="string required form-control"
                        id="Identificacion_Tarjetahabiente" name="Identificacion_Tarjetahabiente" placeholder="Enter Identificacion_Tarjetahabiente"
                        type="text" value="testid">
                <p class="help-block">
                    ID of the card owner.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Numero_Celular</label><input class="string required form-control" id="Numero_Celular"
                        name="Numero_Celular" placeholder="Enter Numero_Celular" type="text" value="32134636868">
                <p class="help-block">
                    Numero_Celular given by Client</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Alias_Tarjeta</label><input class="string required form-control" id="Alias_Tarjeta"
                        name="Alias_Tarjeta" placeholder="Enter Alias_Tarjeta" type="text" value="AHO1">
                <p class="help-block">
                    Alias given by Client</p>
            </div>
            <div class="form-group">
                <label class="control-label">
                    Informacion_Adicional</label><input class="string form-control" id="Informacion_Adicional"
                        name="Informacion_Adicional" placeholder="Enter Informacion_Adicional" type="text">
                <p class="help-block">
                    Additional Information.</p>
            </div>
            <asp:Button runat="server" ID="RemoveCardButton" class="btn btn-default" name="commit"
                type="submit" value="Remove Card" Text="Remove Card" 
                onclick="RemoveCardButton_Click" />
            </form>
        </div>
    </div>
</asp:Content>
