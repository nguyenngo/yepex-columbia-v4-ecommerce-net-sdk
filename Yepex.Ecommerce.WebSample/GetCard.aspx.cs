﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yepex.Ecommerce.SDK.YepexEcommerceService;
using Yepex.Ecommerce.SDK;

namespace Yepex.Ecommerce.WebSample
{
    public partial class GetCard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblErrorPanel.Visible = false;
            lblSuccessPanel.Visible = false;
        }

        protected void GetCardButton_Click(object sender, EventArgs e)
        {
            try
            {
                String IdTransaccion = Request.Form["IdTransaccion"];
                String IdApp = Request.Form["IdApp"];
                String IdentificacionTarjetahabiente = Request.Form["Identificacion_Tarjetahabiente"];
                solicitudObtenerTarjetaEncriptada getCardRequest = new solicitudObtenerTarjetaEncriptada();
                getCardRequest.id_Transaccion = IdTransaccion;
                getCardRequest.id_App = IdApp;
                getCardRequest.identificacion_Tarjetahabiente = IdentificacionTarjetahabiente;


                ServiceContext serviceContext = new ServiceContext();
                respuestaObtenerTarjetaEncriptada getCardResponse = EcommerceCardService.doGetEncryptedCardRequest(serviceContext, getCardRequest);
                lblSuccessPanel.Visible = true;
                lblSuccess.Text = "Web service had been called successful !!!<br>";
                lblSuccess.Text += "Server responses IdApp: " + getCardResponse.id_App + "<br>";
                lblSuccess.Text += "Server responses IdTransaccion: " + getCardResponse.id_Transaccion + "<br>";
                lblSuccess.Text += "Server responses Numero_Tarjeta: " + getCardResponse.numero_Tarjeta + "<br>";
                lblSuccess.Text += "Server responses Fecha_Expiracion: " + getCardResponse.fecha_Expiracion + "<br>";
                lblSuccess.Text += "Server responses Nombre_Tarjetahabiente: " + getCardResponse.nombre_Tarjetahabiente + "<br>";
                lblSuccess.Text += "Server responses Dirreccion_Correspondencia: " + getCardResponse.dirreccion_Correspondencia + "<br>";
                lblSuccess.Text += "Server responses Alias_Tarjeta: " + getCardResponse.alias_Tarjeta + "<br>";
                lblSuccess.Text += "Server responses Numero_Celular: " + getCardResponse.numero_Celular + "<br>";
                lblSuccess.Text += "Server responses Estado: " + getCardResponse.estado + "<br>";

            }
            catch (Exception ex)
            {
                lblErrorPanel.Visible = true;
                lblError.Text = "Cannot send message because : " + ex.Message;
            }
        }
    }
}