﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yepex.Ecommerce.SDK.YepexEcommerceService;
using Yepex.Ecommerce.SDK;

namespace Yepex.Ecommerce.WebSample
{
    public partial class RemoveCard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblErrorPanel.Visible = false;
            lblSuccessPanel.Visible = false;
        }

        protected void RemoveCardButton_Click(object sender, EventArgs e)
        {
            try
            {
                String IdTransaccion = Request.Form["IdTransaccion"];
                String IdApp = Request.Form["IdApp"];
                String IdentificacionTarjetahabiente = Request.Form["Identificacion_Tarjetahabiente"];
                String AliasTarjeta = Request.Form["Alias_Tarjeta"];
                String NumeroCelular = Request.Form["Numero_Celular"];

                solicitudEliminarTarjeta removeCardRequest = new solicitudEliminarTarjeta();
                removeCardRequest.id_Transaccion = IdTransaccion;
                removeCardRequest.id_App = IdApp;
                removeCardRequest.identificacion_Tarjetahabiente = IdentificacionTarjetahabiente;
                removeCardRequest.alias_Tarjeta = AliasTarjeta;
                removeCardRequest.numero_Celular = NumeroCelular;

                ServiceContext serviceContext = new ServiceContext();
                respuestaEliminarTarjeta removeCardResponse = EcommerceCardService.doRemoveCardRequest(serviceContext, removeCardRequest);
                lblSuccessPanel.Visible = true;
                lblSuccess.Text = "Web service had been called successful !!!<br>";
                lblSuccess.Text += "Server responses IdTransaccion: " + removeCardResponse.id_Transaccion + "<br>";
                lblSuccess.Text += "Server responses Estado: " + removeCardResponse.estado + "<br>";
                lblSuccess.Text += "Server responses FechaTransaccion: " + removeCardResponse.fecha_Trasaccion + "<br>";
                lblSuccess.Text += "Server responses CodigoTransaccion: " + removeCardResponse.codigo_Transaccion + "<br>";
            }
            catch (Exception ex)
            {
                lblErrorPanel.Visible = true;
                lblError.Text = "Cannot send message because : " + ex.Message;
            }
        }
    }
}