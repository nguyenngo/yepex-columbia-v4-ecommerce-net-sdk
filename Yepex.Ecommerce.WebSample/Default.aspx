﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Yepex.Ecommerce.WebSample._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1 class="page-header">
        Add Card Service</h1>
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Form Add Card Service</div>
        </div>
        <div class="panel-body">
            <form runat="server" accept-charset="UTF-8" class="simple_form new_user_basic" id="addCardService"
            method="post" novalidate="novalidate">
            <asp:Panel id="lblErrorPanel" runat="server" Visible="false">
                <div class="alert alert-danger">
                    <asp:Label runat="server"  ID="lblError" /></div>
            </asp:Panel>
            <asp:Panel id="lblSuccessPanel" runat="server" Visible="false">
                <div class="alert alert-success">
                    <asp:Label runat="server"  ID="lblSuccess" /></div>
            </asp:Panel>
            <div class="form-group required">
                <label class="required control-label">
                    Id Transaccion</label><input class="string required form-control" id="IdTransaccion"
                        name="IdTransaccion" placeholder="Enter Id Transaccion" type="text" value="1234567890">
                <p class="help-block">
                    An automatic number that identify the transaction, generate by 3rd parties.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Id App</label><input class="string required form-control" id="IdApp" name="IdApp"
                        placeholder="Enter Id App" type="text" value="1">
                <p class="help-block">
                    The Application ID that store the credit card..</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Numero_Tarjeta</label><input class="string required form-control" id="Numero_Tarjeta"
                        name="Numero_Tarjeta" placeholder="Enter Numero_Tarjeta" type="text" value="4005580000099999">
                <p class="help-block">
                    The credit card number.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    CVV</label><input class="string required form-control" id="CVV" name="CVV" placeholder="Enter CVV"
                        type="text" value="999">
                <p class="help-block">
                    The CVV number of the card.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Fecha_Expiracion</label><input class="string required form-control" id="Fecha_Expiracion"
                        name="Fecha_Expiracion" placeholder="Enter Fecha_Expiracion" type="text" value="2018-12">
                <p class="help-block">
                    The date of expiration of the card.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Nombre_Tarjetahabiente</label><input class="string required form-control" id="Nombre_Tarjetahabiente"
                        name="Nombre_Tarjetahabiente" placeholder="Enter Nombre_Tarjetahabiente" type="text"
                        value="test_name">
                <p class="help-block">
                    Full Name of the card owner.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Identificacion_Tarjetahabiente</label><input class="string required form-control"
                        id="Identificacion_Tarjetahabiente" name="Identificacion_Tarjetahabiente" placeholder="Enter Identificacion_Tarjetahabiente"
                        type="text" value="test_id">
                <p class="help-block">
                    ID of the card owner.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Alias_Tarjeta</label><input class="string required form-control" id="Alias_Tarjeta"
                        name="Alias_Tarjeta" placeholder="Enter Alias_Tarjeta" type="text" value="AHO1">
                <p class="help-block">
                    Alias given by Client</p>
            </div>
            <div class="form-group">
                <label class="required control-label">
                    Dirreccion_Correspondencia</label><input class="string form-control" id="Dirreccion_Correspondencia"
                        name="Dirreccion_Correspondencia" placeholder="Enter Dirreccion_Correspondencia"
                        type="text" value="This is fake data test">
                <p class="help-block">
                    Address of the card owner.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Numero_Celular</label><input class="string required form-control" id="Numero_Celular"
                        name="Numero_Celular" placeholder="Enter Numero_Celular" type="text" value="32134636868">
                <p class="help-block">
                    The Cellphone number of card owner.</p>
            </div>
            <div class="form-group required">
                <label class="required control-label">
                    Marca</label><input class="string required form-control" id="Text1"
                        name="Marca" placeholder="Enter Marca" type="text" value="Test_Marca">
                 <p class="help-block">
                    The marce of card.</p>
            </div>
            <div class="form-group">
                <label class="control-label">
                    Informacion_Adicional</label><input class="string form-control" id="Informacion_Adicional"
                        name="Informacion_Adicional" placeholder="Enter Informacion_Adicional" type="text">
                <p class="help-block">
                    Additional Information.</p>
            </div>
            <asp:Button runat="server" ID="AddCardButton" class="btn btn-default" name="commit"
                type="submit" value="Add Card" OnClick="AddCardButton_Click" Text="Add Card" />
            </form>
        </div>
    </div>
</asp:Content>
