﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Help.aspx.cs" Inherits="Yepex.Ecommerce.WebSample.Help" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="page-header">
        Help</h1>
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Help</div>
        </div>
        <div class="panel-body">
            <div class="readme file wiki-content">
                <h2 id="markdown-header-yp-yepex-ecommerce-sdk-for-net">
                    YP Yepex eCommerce SDK for .NET</h2>
                <p>
                    This .NET SDK is used to integrate with Yepex eCommerce solution provided by YellowPepper.
                </p>
                <h3 id="markdown-header-prerequisites">
                    Prerequisites</h3>
                <ul>
                    <li>Visual Studio 2010 or higher</li>
                </ul>
                <h3 id="markdown-header-sdk-reference">
                    SDK Reference</h3>
                <p>
                    Once all the library are downloaded, simply add the following libraries to your
                    project references (Project &gt; Add Reference...):</p>
                <ul>
                    <li>Yepex.Ecommerce.SDK.dll</li>
                </ul>
                <h3 id="markdown-header-configure-your-application">
                    Configure Your Application</h3>
                <p>
                    When using the SDK with your application, the SDK will attempt to look for YellowPepper-specific
                    settings in your application's <strong>app.config</strong> or <strong>web.config</strong>
                    file.</p>
                <div class="codehilite">
                    <pre><span class="nt">&lt;configuration&gt;</span>
  <span class="nt">&lt;configSections&gt;</span>
    <span class="nt">&lt;section</span> <span class="na">name=</span><span class="s">"yellowpepper"</span> <span
        class="na">type=</span><span class="s">"System.Configuration.NameValueSectionHandler"</span> <span
            class="nt">/&gt;</span>
  <span class="nt">&lt;/configSections&gt;</span>
  <span class="c">&lt;!-- YellowPepper SDK config --&gt;</span>
  <span class="nt">&lt;yellowpepper&gt;</span>
    <span class="nt">&lt;add</span> <span class="na">key=</span><span class="s">"ecommerceWsUrl"</span> <span
        class="na">value=</span><span class="s">"https://{hostname}/co/yw4/eCommerceWebService"</span> <span
            class="nt">/&gt;</span>
    <span class="nt">&lt;add</span> <span class="na">key=</span><span class="s">"ecommerceWsRSAKeyType"</span> <span
        class="na">value=</span><span class="s">"AES_ENCRYPTED"</span> <span class="nt">/&gt;</span>
    <span class="nt">&lt;add</span> <span class="na">key=</span><span class="s">"ecommerceWsRSAKeyEncrypted"</span> <span
        class="na">value=</span><span class="s">"3UOFt5yP9708g/suf...........azBr3UWb8/5WjjlF+SI="</span> <span
            class="nt">/&gt;</span>
    <span class="nt">&lt;add</span> <span class="na">key=</span><span class="s">"ecommerceWsRSAInitCode"</span> <span
        class="na">value=</span><span class="s">"AOCAg8........EAp65Tb"</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;/yellowpepper&gt;</span>
  <span class="nt">&lt;system.web&gt;</span>
    <span class="nt">&lt;compilation</span> <span class="na">debug=</span><span class="s">"true"</span> <span
        class="na">targetFramework=</span><span class="s">"4.0"</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;/system.web&gt;</span>
  <span class="nt">&lt;system.webServer&gt;</span>
    <span class="nt">&lt;modules</span> <span class="na">runAllManagedModulesForAllRequests=</span><span
        class="s">"true"</span> <span class="nt">/&gt;</span>
  <span class="nt">&lt;/system.webServer&gt;</span>
  <span class="nt">&lt;system.serviceModel&gt;</span>
    <span class="nt">&lt;bindings&gt;</span>
      <span class="nt">&lt;basicHttpBinding&gt;</span>
        <span class="nt">&lt;binding</span> <span class="na">name=</span><span class="s">"eCommerceWebServiceServiceSoapBinding"</span><span
            class="nt">&gt;</span>
          <span class="nt">&lt;security</span> <span class="na">mode=</span><span class="s">"Transport"</span> <span
              class="nt">/&gt;</span>
        <span class="nt">&lt;/binding&gt;</span>
        <span class="nt">&lt;binding</span> <span class="na">name=</span><span class="s">"eCommerceWebServiceServiceSoapBinding1"</span> <span
            class="nt">/&gt;</span>
      <span class="nt">&lt;/basicHttpBinding&gt;</span>
    <span class="nt">&lt;/bindings&gt;</span>
    <span class="nt">&lt;client&gt;</span>
      <span class="nt">&lt;endpoint</span> <span class="na">address=</span><span class="s">"https://{hostname}/co/yw4/eCommerceWebService"</span> <span
          class="na">binding=</span><span class="s">"basicHttpBinding"</span> <span class="na">bindingConfiguration=</span><span
              class="s">"eCommerceWebServiceServiceSoapBinding"</span> <span class="na">contract=</span><span
                  class="s">"YepexEcommerceService.ServicioWeb_eCommerce"</span> <span class="na">name=</span><span
                      class="s">"PuertoServicioWeb_eCommerce"</span> <span class="nt">/&gt;</span>
    <span class="nt">&lt;/client&gt;</span>
  <span class="nt">&lt;/system.serviceModel&gt;</span>
<span class="nt">&lt;/configuration&gt;</span>
</pre>
                </div>
                <p>
                    The following are values that can be specified in the <code>&lt;yellowpepper&gt;</code>
                    section of the <strong>app.config</strong> or <strong>web.config</strong> file:</p>
                <table>
                    <thead>
                        <tr>
                            <th>
                                Name
                            </th>
                            <th>
                                Description
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <code>ecommerceWsUrl</code>
                            </td>
                            <td>
                                Yepex eCommerce URL which published by YellowPepper..
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <code>ecommerceWsRSAKeyType</code>
                            </td>
                            <td>
                                The Key Encrypted Protocol. Default is AES_ENCRYPTED.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <code>ecommerceWsRSAKeyEncrypted</code>
                            </td>
                            <td>
                                The encrypted text of RSA Public key. As secuirity required, we need to protect
                                RSA public key by encrypting it using AES or 3DES before storing in persistence
                                SDK will decrypt this key to encrypt credit card before sending it to Yepex eCommerce
                                API..
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <code>ecommerceWsRSAInitCode</code>
                            </td>
                            <td>
                                The AES Encryption Key which will be used to encrypt RSA Public Key from YellowPepper
                                before storing it as a setting in persistence storage..
                            </td>
                        </tr>
                    </tbody>
                </table>
                <h3 id="markdown-header-key-security-description">
                    Key Security Description</h3>
                <ul>
                    <li>For Add Card Request, user's credit card must be encrypted by RSA Algorithm before
                        sending to payment server. The RSA Plain Public key is provided by Payment System,
                        YellowPepper is forwarding this key only and cannot decrypt for security required.
                        More security, in SDK, the RSA Public Key must be encrypted one more time by using
                        YP specified security mechanism. It will protect the key from any hijacker. The
                        key flow is describe as below:</li>
                    <li>Client receive the RSA plain public key from YellowPepper (It's X509 key and format
                        in Base64 mode)</li>
                    <li>Client must encrypt this RSA plain public key by using Key Encryption Application
                        ( see in below section) or using web sample setting page. In this way, Client generate
                        the Encryption Key SDK will return the RSA Encryption String in Base64 format. Client
                        should stores all these information client database for further action.</li>
                    <li>When send card to server, client must provide two factor: RSA Encrypted Public Key
                        and Encrypting Key. SDK will process and send data to server. These key will be
                        used only by SDK for commnunication. Other apps cannot decrypt or do any action.</li>
                </ul>
                <h3 id="markdown-header-to-make-an-api-call">
                    To make an API call:</h3>
                <ul>
                    <li>Import the library from into your code. For example,</li>
                </ul>
                <div class="codehilite">
                    <pre><span class="kn">import</span> <span class="nn">com.yellowpepper.ecommerce.api.wsclient.RespuestaAgregarTarjeta</span><span
                        class="o">;</span>
<span class="kn">import</span> <span class="nn">com.yellowpepper.ecommerce.api.wsclient.SolicitudAgregarTarjeta</span><span
    class="o">;</span>
<span class="kn">import</span> <span class="nn">com.yellowpepper.ecommerce.api.service.EcommerceCardService</span><span
    class="o">;</span>
<span class="o">...</span>
</pre>
                </div>
                <ul>
                    <li>Load config from map of configuration</li>
                </ul>
                <div class="codehilite">
                    <pre><span class="n">using</span> <span class="n">Yepex</span><span class="o">.</span><span
                        class="na">Ecommerce</span><span class="o">.</span><span class="na">SDK</span><span
                            class="o">.</span><span class="na">YepexEcommerceService</span><span class="o">;</span>
<span class="n">using</span> <span class="n">Yepex</span><span class="o">.</span><span
    class="na">Ecommerce</span><span class="o">.</span><span class="na">SDK</span><span
        class="o">;</span>
<span class="o">...</span>
</pre>
                </div>
                <ul>
                    <li>Call the service for each action: </li>
                </ul>
                <div class="codehilite">
                    <pre><span class="n">ServiceContext</span> <span class="n">serviceContext</span> <span
                        class="o">=</span> <span class="k">new</span> <span class="n">ServiceContext</span><span
                            class="o">();</span>
<span class="n">respuestaAgregarTarjeta</span> <span class="n">addCardResponse</span> <span
    class="o">=</span> <span class="n">EcommerceCardService</span><span class="o">.</span><span
        class="na">doAddCardRequest</span><span class="o">(</span><span class="n">serviceContext</span><span
            class="o">,</span> <span class="n">addCardRequest</span><span class="o">);</span>
<span class="o">....</span>
</pre>
                </div>
                <ul>
                    <li>Parse response for later action:</li>
                </ul>
                <div class="codehilite">
                    <pre><span class="n">lblSuccess</span><span class="o">.</span><span class="na">Text</span> <span
                        class="o">=</span> <span class="s">"Web service had been called successful !!!&lt;br&gt;"</span><span
                            class="o">;</span>
<span class="n">lblSuccess</span><span class="o">.</span><span class="na">Text</span> <span
    class="o">+=</span> <span class="s">"Server responses IdTransaccion: "</span> <span
        class="o">+</span> <span class="n">addCardResponse</span><span class="o">.</span><span
            class="na">id_Transaccion</span> <span class="o">+</span> <span class="s">"&lt;br&gt;"</span><span
                class="o">;</span>
<span class="n">lblSuccess</span><span class="o">.</span><span class="na">Text</span> <span
    class="o">+=</span> <span class="s">"Server responses Estado: "</span> <span class="o">+</span> <span
        class="n">addCardResponse</span><span class="o">.</span><span class="na">estado</span> <span
            class="o">+</span> <span class="s">"&lt;br&gt;"</span><span class="o">;</span>
<span class="n">lblSuccess</span><span class="o">.</span><span class="na">Text</span> <span
    class="o">+=</span> <span class="s">"Server responses FechaTransaccion: "</span> <span
        class="o">+</span> <span class="n">addCardResponse</span><span class="o">.</span><span
            class="na">fecha_Transaccion</span> <span class="o">+</span> <span class="s">"&lt;br&gt;"</span><span
                class="o">;</span>
<span class="n">lblSuccess</span><span class="o">.</span><span class="na">Text</span> <span
    class="o">+=</span> <span class="s">"Server responses CodigoTransaccion: "</span> <span
        class="o">+</span> <span class="n">addCardResponse</span><span class="o">.</span><span
            class="na">codigo_Transaccion</span> <span class="o">+</span> <span class="s">"&lt;br&gt;"</span><span
                class="o">;</span>
</pre>
                </div>
                <h3 id="markdown-header-ecommerce-service-definition">
                    Ecommerce Service Definition</h3>
                <p>
                    In SDK we support five methods to call YellowPepper webservice: * Add Card Service:
                    Add new cards on TT data bases,Before send data to payment system. SDK will encrypted
                    the credit card number because of PCI-DSS requirement.</p>
                <div class="codehilite">
                    <pre><span class="n">ServiceContext</span> <span class="n">serviceContext</span> <span
                        class="o">=</span> <span class="k">new</span> <span class="n">ServiceContext</span><span
                            class="o">();</span>
<span class="n">respuestaAgregarTarjeta</span> <span class="n">addCardResponse</span> <span
    class="o">=</span> <span class="n">EcommerceCardService</span><span class="o">.</span><span
        class="na">doAddCardRequest</span><span class="o">(</span><span class="n">serviceContext</span><span
            class="o">,</span> <span class="n">addCardRequest</span><span class="o">);</span>
<span class="o">....</span>
</pre>
                </div>
                <ul>
                    <li>Remove Card Service: The main function of this method is to allow the commerce remove
                        credit cards on the repository.</li>
                </ul>
                <div class="codehilite">
                    <pre><span class="n">ServiceContext</span> <span class="n">serviceContext</span> <span
                        class="o">=</span> <span class="k">new</span> <span class="n">ServiceContext</span><span
                            class="o">();</span>
<span class="n">respuestaEliminarTarjeta</span> <span class="n">removeCardResponse</span> <span
    class="o">=</span> <span class="n">EcommerceCardService</span><span class="o">.</span><span
        class="na">doRemoveCardRequest</span><span class="o">(</span><span class="n">serviceContext</span><span
            class="o">,</span> <span class="n">removeCardRequest</span><span class="o">);</span>
<span class="o">....</span>
</pre>
                </div>
                <ul>
                    <li>Get Card Alias Service: Enable the user of those third party apps to get information
                        of the Alias of the cards previously registered by their users using the method
                        AddCard (ALIAS = name given by the user to identify one its cards). </li>
                </ul>
                <div class="codehilite">
                    <pre><span class="n">ServiceContext</span> <span class="n">serviceContext</span> <span
                        class="o">=</span> <span class="k">new</span> <span class="n">ServiceContext</span><span
                            class="o">();</span>
<span class="n">respuestaObtenerAlias</span> <span class="n">getCardAliasResponse</span> <span
    class="o">=</span> <span class="n">EcommerceCardService</span><span class="o">.</span><span
        class="na">doGetCardAliasRequest</span><span class="o">(</span><span class="n">serviceContext</span><span
            class="o">,</span> <span class="n">getCardAliasRequest</span><span class="o">);</span>
<span class="o">....</span>
</pre>
                </div>
                <ul>
                    <li>Get Card Service: Enables the merchant to request the card information ( card #
                        &amp; expiry date) of a card previously registered in the repository (TT).</li>
                </ul>
                <div class="codehilite">
                    <pre><span class="n">ServiceContext</span> <span class="n">serviceContext</span> <span
                        class="o">=</span> <span class="k">new</span> <span class="n">ServiceContext</span><span
                            class="o">();</span>
<span class="n">respuestaObtenerTarjetaEncriptada</span> <span class="n">getCardResponse</span> <span
    class="o">=</span> <span class="n">EcommerceCardService</span><span class="o">.</span><span
        class="na">doGetEncryptedCardRequest</span><span class="o">(</span><span class="n">serviceContext</span><span
            class="o">,</span> <span class="n">getCardRequest</span><span class="o">);</span>
<span class="o">....</span>
</pre>
                </div>
                <ul>
                    <li>Payment Service: The main function of this method is to enable the merchant to implement
                        a payment using a credit card saved on the TT.</li>
                </ul>
                <div class="codehilite">
                    <pre><span class="n">ServiceContext</span> <span class="n">serviceContext</span> <span
                        class="o">=</span> <span class="k">new</span> <span class="n">ServiceContext</span><span
                            class="o">();</span>
<span class="n">respuestaSolicitudPago</span> <span class="n">paymentResponse</span> <span
    class="o">=</span> <span class="n">EcommercePaymentService</span><span class="o">.</span><span
        class="na">doPaymentRequest</span><span class="o">(</span><span class="n">serviceContext</span><span
            class="o">,</span> <span class="n">paymentRequest</span><span class="o">);</span>
<span class="o">....</span>
</pre>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
