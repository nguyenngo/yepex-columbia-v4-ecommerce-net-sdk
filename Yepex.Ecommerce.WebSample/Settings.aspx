﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Settings.aspx.cs" Inherits="Yepex.Ecommerce.WebSample.Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="page-header">
        Settings</h1>
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Settings</div>
        </div>
        <div class="panel-body">
            <form accept-charset="UTF-8" runat="server" class="simple_form new_user_basic"
            id="settingService" method="post" novalidate="novalidate">
            <asp:Panel id="lblErrorPanel" runat="server" Visible="false">
                <div class="alert alert-danger">
                    <asp:Label runat="server"  ID="lblError" /></div>
            </asp:Panel>
            <asp:Panel id="lblGenerateSuccessPanel" runat="server" Visible="false">
                <div class="alert alert-success">
                    <asp:Label runat="server"  ID="lblGenerateSuccess" Text="New RSA Encrypted had been generated and updated successul."/></div>
            </asp:Panel>
            <asp:Panel id="lblUpdateSuccessPanel" runat="server" Visible="false">
                <div class="alert alert-success">
                    <asp:Label runat="server"  ID="lblUpdateSuccess" Text="All settings had been updated successful."/></div>
            </asp:Panel>
            <div class="form-group required">
                <div class="col-sm-2">
                    <label class="required control-label">
                        Yepex eCommerce Url:</label>
                </div>
                <div class="col-sm-10">
                    <asp:TextBox CssClass="string required form-control" id="txtWebServiceURL" runat="server" />
                    <p class="help-block">
                        Yepex eCommerce URL which published by YellowPepper.</p>
                </div>
            </div>
            <div class="form-group required">
                <div class="col-sm-2">
                    <label class="required control-label">
                        AES Encryption Key:</label>
                </div>
                <div class="col-sm-10">
                    <asp:TextBox CssClass="string required form-control" id="txtRSAInitCode" runat="server" />
                    <p class="help-block">
                        The AES Encryption Key which will be used to encrypt RSA Public Key from YellowPepper
                        before storing it as a setting in persistence storage.
                    </p>
                </div>
            </div>
            <div class="form-group required">
                <div class="col-sm-2">
                    <label class="required control-label">
                        RSA Public Key (Encrypted & Base64):</label>
                </div>
                <div class="col-sm-10">
                    <asp:TextBox CssClass="string required form-control" id="txtRSAEncrypted" runat="server" />
                    <p class="help-block">
                        The encrypted text of RSA Public key. As secuirity required, we need to protect
                        RSA public key by encrypting it using AES or 3DES before storing in persistence
                        SDK will decrypt this key to encrypt credit card before sending it to Yepex eCommerce
                        API.
                    </p>
                </div>
            </div>
            <asp:Button runat="server" ID="UpdateButton" class="btn btn-default" name="commit"
                type="submit" value="Update" Text="Update" onclick="UpdateButton_Click" />
            <div class="hr">
                <hr />
            </div>
            <div class="col-sm-12">
                <label class="required control-label">
                    If you don't have the text of encrypted RSA public key, use below method to generate
                    it from plan key (Provided by YellowPepper)</label><br />
                <br />
            </div>
            <div class="form-group required">
                <div class="col-sm-2">
                    <label class="required control-label">
                        AES Encryption Key:</label>
                </div>
                <div class="col-sm-10">
                   <asp:TextBox CssClass="string required form-control" id="txtRSAInitCodeGenerate" runat="server" />
                    <p class="help-block">
                        The AES Encryption Key which will be used for protecting RSA Public Key from YP.
                    </p>
                </div>
            </div>
            <div class="form-group required">
                <div class="col-sm-2">
                    <label class="required control-label">
                        RSA Public Key (Base64 format):</label>
                </div>
                <div class="col-sm-10">
                    <asp:TextBox CssClass="string required form-control" id="txtRSAPlainKeyGenerate" runat="server" />
                    <p class="help-block">
                        The plain RSA Public key (format Base64) and provided by YellowPepper. You must
                        encrypt this plain text key before storing it as a settings or using in SDK because
                        of security required. After "Generate and Updated", the RSA Public key will be shown
                        in RSA Public Key (Encrypted & Base64) text box If you don't have this information,
                        contact YellowPepper for details
                    </p>
                </div>
            </div>
            <asp:Button runat="server" ID="GenerateUpdateButton" class="btn btn-default" name="commit"
                type="submit" value="GenerateUpdate" Text="Generate and Update" 
                onclick="GenerateUpdateButton_Click" />
            </form>
        </div>
    </div>
</asp:Content>
