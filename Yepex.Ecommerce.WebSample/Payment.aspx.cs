﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yepex.Ecommerce.SDK.YepexEcommerceService;
using Yepex.Ecommerce.SDK;

namespace Yepex.Ecommerce.WebSample
{
    public partial class Payment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblErrorPanel.Visible = false;
            lblSuccessPanel.Visible = false;
        }

        protected void PaymentButton_Click(object sender, EventArgs e)
        {
            try
            {
                String IdTransaccion = Request.Form["IdTransaccion"];
                String IdApp = Request.Form["IdApp"];
                String AliasTarjeta = Request.Form["Alias_Tarjeta"];
                String ValorCompra = Request.Form["Valor_Compra"];
                String Valor_Propina = Request.Form["Valor_Propina"];
                String DescripcionPago = Request.Form["Descripcion_Pago"];
                String ReferenciaPago = Request.Form["Referencia_Pago"];
                String IVA = Request.Form["IVA"];
                String baseDevolucionIVA = Request.Form["baseDevolucionIVA"];
                String propina = Request.Form["propina"];
                String IAC = Request.Form["IAC"];
                String tasaAeroportuaria = Request.Form["tasaAeroportuaria"];
                String Cuotas = Request.Form["Cuotas"];

                solicitudPago paymentRequest = new solicitudPago();
                paymentRequest.id_Transaccion = IdTransaccion;
                paymentRequest.id_App = IdApp;
                paymentRequest.alias_Tarjeta = AliasTarjeta;
                paymentRequest.valor_Compra = ValorCompra ;
                
                paymentRequest.descripcion_Pago = DescripcionPago;
                paymentRequest.referencia_Pago = ReferenciaPago;
                paymentRequest.IVA  = IVA;
                paymentRequest.base_Devolucion_IVA = baseDevolucionIVA;
                paymentRequest.propina = propina;
                paymentRequest.IAC = IAC;
                paymentRequest.tasa_Aeroportuaria = tasaAeroportuaria;
                paymentRequest.cuotas = Cuotas;

                ServiceContext serviceContext = new ServiceContext();
                respuestaSolicitudPago paymentResponse = EcommercePaymentService.doPaymentRequest(serviceContext, paymentRequest);
                lblSuccessPanel.Visible = true;
                lblSuccess.Text = "Web service had been called successful !!!<br>";
                lblSuccess.Text += "Server responses IdTransaccion: " + paymentResponse.id_Transaccion + "<br>";
                lblSuccess.Text += "Server responses CodigoTransaccion: " + paymentResponse.codigo_Transaccion + "<br>";
                lblSuccess.Text += "Server responses Fecha_Trasaccion: " + paymentResponse.fecha_Trasaccion + "<br>";
                lblSuccess.Text += "Server responses Estado: " + paymentResponse.estado + "<br>";

            }
            catch (Exception ex)
            {
                lblErrorPanel.Visible = true;
                lblError.Text = "Cannot send message because : " + ex.Message;
            }
        }
    }
}