YP Yepex eCommerce SDK for .NET
----------------------------------------
This .NET SDK is used to integrate with Yepex eCommerce solution provided by YellowPepper. 

## Prerequisites

* Visual Studio 2010 or higher

## SDK Reference
Once all the library are downloaded, simply add the following libraries to your project references (Project > Add Reference...):

* Yepex.Ecommerce.SDK.dll

## Configure Your Application
When using the SDK with your application, the SDK will attempt to look for YellowPepper-specific settings in your application's **app.config** or **web.config** file.

````xml
<configuration>
  <configSections>
    <section name="yellowpepper" type="System.Configuration.NameValueSectionHandler" />
  </configSections>
  <!-- YellowPepper SDK config -->
  <yellowpepper>
    <add key="ecommerceWsUrl" value="https://{hostname}/co/yw4/eCommerceWebService" />
  </yellowpepper>
  <system.web>
    <compilation debug="true" targetFramework="4.0" />
  </system.web>
  <system.webServer>
    <modules runAllManagedModulesForAllRequests="true" />
  </system.webServer>
  <system.serviceModel>
    <bindings>
      <basicHttpBinding>
        <binding name="eCommerceWebServiceServiceSoapBinding">
          <security mode="Transport" />
        </binding>
        <binding name="eCommerceWebServiceServiceSoapBinding1" />
      </basicHttpBinding>
    </bindings>
    <client>
      <endpoint address="https://{hostname}/co/yw4/eCommerceWebService" binding="basicHttpBinding" bindingConfiguration="eCommerceWebServiceServiceSoapBinding" contract="YepexEcommerceService.ServicioWeb_eCommerce" name="PuertoServicioWeb_eCommerce" />
    </client>
  </system.serviceModel>
</configuration>
````
The following are values that can be specified in the `<yellowpepper>` section of the **app.config** or **web.config** file:

| Name | Description |
| ---- | ----------- |
| `ecommerceWsUrl` | Yepex eCommerce URL which published by YellowPepper.. |

##To make an API call:

*	Import the library from  into your code. For example,

~~~java
import com.yellowpepper.ecommerce.api.wsclient.RespuestaAgregarTarjeta;
import com.yellowpepper.ecommerce.api.wsclient.SolicitudAgregarTarjeta;
import com.yellowpepper.ecommerce.api.service.EcommerceCardService;
...
~~~

*	Load config from map of configuration

~~~java	
using Yepex.Ecommerce.SDK.YepexEcommerceService;
using Yepex.Ecommerce.SDK;
...
~~~

*	Call the service for each action: 

~~~java
ServiceContext serviceContext = new ServiceContext();
respuestaAgregarTarjeta addCardResponse = EcommerceCardService.doAddCardRequest(serviceContext, addCardRequest);
....
~~~

*	Parse response for later action:

~~~java	
lblSuccess.Text = "Web service had been called successful !!!<br>";
lblSuccess.Text += "Server responses IdTransaccion: " + addCardResponse.id_Transaccion + "<br>";
lblSuccess.Text += "Server responses Estado: " + addCardResponse.estado + "<br>";
lblSuccess.Text += "Server responses FechaTransaccion: " + addCardResponse.fecha_Transaccion + "<br>";
lblSuccess.Text += "Server responses CodigoTransaccion: " + addCardResponse.codigo_Transaccion + "<br>";
~~~
		
##Ecommerce Service Definition
In SDK we support five methods to call YellowPepper webservice:
###Add Card Service: 
Add new cards on TT data bases.

~~~java
ServiceContext serviceContext = new ServiceContext();
respuestaAgregarTarjeta addCardResponse = EcommerceCardService.doAddCardRequest(serviceContext, addCardRequest);
....
~~~

It's a service that receives this arguments:

| Params name | Description                    | Sample                    |
| ------------- | ------------------------------ |
| `ID_Transaccion`      | An automatic number that identify the transaction, generate by 3rd parties’     | 
| `ID_App`      | The Application ID that store the credit card      | 
| `Numero_Tarjeta`      | The credit card number      | 
| `CVV`      | The CVV number of the card      | 
| `Marca`      | Brand: This value has to be sent by the third party      | 
| `Fecha_Expiracion`      | The date of expiration of the card      | 
| `Nombre_Tarjetahabiente`      | Full Name of the card owner      | 
| `Identificacion_Tarjetahabiente`      | ID of the card owner      | 
| `Alias_Tarjeta`      | Alias given by Client      | 
| `Dirreccion_Correspondencia`      | Address of the card owner      | 
| `Numero_Celular`      | The Cellphone number of card owner      | 
| `Informacion_Adicional`      | Additional Information      | 

And the response data has below elements:

| Data name | Description                    |
| ------------- | ------------------------------ |
| `ID_Transaccion`      | An automatic number that identify the transaction |
| `Codigo_Transaccion`      | Identify the status of the transaction (see table) |
| `Estado`      | Description of the transaction, if was an error, we give the complete description of this one. |
| `Fecha_Trasaccion`      | Date of the Trx. |

###Remove Card Service: 
The main function of this method is to allow the commerce remove credit cards on the repository.

~~~java
ServiceContext serviceContext = new ServiceContext();
respuestaEliminarTarjeta removeCardResponse = EcommerceCardService.doRemoveCardRequest(serviceContext, removeCardRequest);
....
~~~

It's a service that receives this arguments:

| Params name | Description                    |
| ------------- | ------------------------------ |
| `ID_Transaccion`      | An automatic number that identify the transaction, generate by 3rd parties’     | 
| `ID_App`      | The Application ID that store the credit card      | 
| `Identificacion_Tarjetahabiente`      | ID of the card owner      | 
| `Alias_Tarjeta`      | Alias given by Client      | 
| `Numero_Celular`      | The Cellphone number of card owner      | 
| `Informacion_Adicional`      | Additional Information      | 

And the response data has below elements:

| Data name | Description                    |
| ------------- | ------------------------------ |
| `ID_Transaccion`      | An automatic number that identify the transaction |
| `Codigo_Transaccion`      | Identify the status of the transaction (see table) |
| `Estado`      | Description of the transaction, if was an error, we give the complete description of this one. |
| `Fecha_Trasaccion`      | Date of the Trx. |

###Get Card Alias Service:  
Enable the user of those third party apps to get information of the Alias of the cards previously registered by their users using the method AddCard (ALIAS = name given by the user to identify one its cards). 

~~~java
ServiceContext serviceContext = new ServiceContext();
respuestaObtenerAlias getCardAliasResponse = EcommerceCardService.doGetCardAliasRequest(serviceContext, getCardAliasRequest);
....
~~~

It's a service that receives this arguments:

| Params name | Description                    |
| ------------- | ------------------------------ |
| `ID_Transaccion`      | An automatic number that identify the transaction, generate by 3rd parties’     | 
| `ID_App`      | The Application ID that store the credit card      | 
| `Identificacion_Tarjetahabiente`      | ID of the card owner      | 
| `Informacion_Adicional`      | Additional Information      | 


And the response data has below elements:

| Data name | Description                    |
| ------------- | ------------------------------ |
| `ID_Transaccion`      | An automatic number that identify the transaction |
| `ID_App`      | The Application ID that store the credit card      | 
| `Identificacion_Tarjetahabiente`      | ID of the card owner      | 
| `Alias_Tarjeta`      | Alias given by Client      | 
| `Codigo_Transaccion`      | Identify the status of the transaction (see table) |
| `Estado`      | Description of the transaction, if was an error, we give the complete description of this one. |
| `Informacion_Adicional`      | Additional Information      | 

###Get Card Service: 
Enables the merchant to request the card information ( card # & expiry date) of a card previously registered in the repository (TT).

~~~java
ServiceContext serviceContext = new ServiceContext();
respuestaObtenerTarjetaEncriptada getCardResponse = EcommerceCardService.doGetEncryptedCardRequest(serviceContext, getCardRequest);
....
~~~

It's a service that receives this arguments:

| Params name | Description                    |
| ------------- | ------------------------------ |
| `ID_Transaccion`      | An automatic number that identify the transaction, generate by 3rd parties’     | 
| `ID_App`      | The Application ID that store the credit card      | 
| `Identificacion_Tarjetahabiente`      | ID of the card owner      | 
| `Alias_Tarjeta`      | Alias given by Client      | 
| `Informacion_Adicional`      | Additional Information      | 

And the response data has below elements:

| Data name | Description                    |
| ------------- | ------------------------------ |
| `ID_Transaccion`      | An automatic number that identify the transaction |
| `ID_App`      | The Application ID that store the credit card      | 
| `Numero_Tarjeta`      | The credit card number      | 
| `Fecha_Expiracion`      | The date of expiration of the card      | 
| `Nombre_Tarjetahabiente`      | Full Name of the card owner      | 
| `Identificacion_Tarjetahabiente`      | ID of the card owner      | 
| `Dirreccion_Correspondencia`      | Address of the card owner      | 
| `Alias_Tarjeta`      | Alias given by Client      | 
| `Numero_Celular`      | The Cellphone number of card owner      | 
| `Codigo_Transaccion`      | Identify the status of the transaction (see table) |
| `Estado`      | Description of the transaction, if was an error, we give the complete description of this one. |
| `Informacion_Adicional`      | Additional Information      | 

###Payment Service: 
The main function of this method is to enable the merchant to implement a payment using a credit card saved on the TT.

~~~java
ServiceContext serviceContext = new ServiceContext();
respuestaSolicitudPago paymentResponse = EcommercePaymentService.doPaymentRequest(serviceContext, paymentRequest);
....
~~~

It's a service that receives this arguments:

| Params name | Description                    |
| ------------- | ------------------------------ |
| `ID_Transaccion`      | An automatic number that identify the transaction, generate by 3rd parties’     | 
| `ID_App`      | The Application ID that store the credit card      | 
| `Identificacion_Tarjetahabiente`      | ID of the card owner      | 
| `Alias_Tarjeta`      | Alias given by Client      |
| `Valor_Compra`      | Value of the purchase.     | 
| `Descripcion_Pago`      | Description of the content purchase.    | 
| `Referencia_Pago`      | External reference used on receipt.    | 
| `Cuotas`      | Quota, a limited or fixed number or amount    | 
| `IVA`      | VAT, value added tax    | 
| `Base_Devolucion_IVA`      | Base Tax Return Value    | 
| `Propina`      | Value of the tip.    | 
| `IAC`      | the value of IAC tax    | 
| `Tasa_Aeroportuaria`      | Airport Tax, A tax levied on passengers for passing through an airport.    | 
| `Informacion_Adicional`      | Additional Information      | 

And the response data has below elements:

| Data name | Description                    |
| ------------- | ------------------------------ |
| `ID_Transaccion`      | An automatic number that identify the transaction |
| `Codigo_Transaccion`      | Identify the status of the transaction (see table) |
| `Estado`      | Description of the transaction, if was an error, we give the complete description of this one. |
| `Fecha_Trasaccion`      | Date of the Trx. |

###Tax definition: 
When add new card and perform the payment, we have some specialist the value of tax. The definition and sample can be refer via below table:

| Tax Value | Description                    | Length | Ecommerce Reference | Sample | 
| ------------- | ------------------------------ | ------------- | ------------- | ------------- |
| `IVA`      | VAT, value added tax    | Numeric (10 digits - last 2 decimals) | It's part of the purchase data string, field IVA | 10'899.678,99 or 1089967899 |
| `Base_Devolucion_IVA`      | Base Tax Return Value    | Numeric (10 digits - last 2 decimals) | It's part of the purchase data string, field IVAReturn | 10'899.678,99 or 1089967899 |
| `Propina`      | Value of the tip.    | Numeric (10 digits - last 2 decimals) | It's part the taxes string, ID = 3, name = PROPINA, amount=1089967899 | 10'899.678,99 or 1089967899 |
| `IAC`      | the value of IAC tax    | Numeric (10 digits - last 2 decimals) | It's part the taxes string, ID = 6, name = IAC, amount=1089967899 | 10'899.678,99 or 1089967899 |
| `Tasa_Aeroportuaria`      | Airport Tax, A tax levied on passengers for passing through an airport.    | Numeric (10 digits - last 2 decimals) | It's part the taxes string, ID = 4, name = TASA_AEREO, amount=1089967899 | 10'899.678,99 or 1089967899 |
| `Cuotas`      | Quota, a limited or fixed number or amount    | Numeric 3 digits | It's part of the purchase data string, field quotaId | 002 |
| `Marca`      | Brand: This value has to be sent by the third party      |  Char | It's part of CardData String, field brand | VISA, MASTERCARD, AMEX, DINERS, CREDENCIAl|