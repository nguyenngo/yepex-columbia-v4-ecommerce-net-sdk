﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yepex.Ecommerce.SDK
{
    public class SDKVersion
    {
        /// <summary>
        /// SDK ID used in User-Agent HTTP header
        /// </summary>
        /// <returns>SDK ID</returns>
        public static string GetSDKId() { return "YepexEcommerceSDK"; }

        /// <summary>
        /// SDK Version used version
        /// </summary>
        /// <returns>SDK Version</returns>
        public static string GetSDKVersion() { return BaseConstants.SdkVersion; }
    }
}
