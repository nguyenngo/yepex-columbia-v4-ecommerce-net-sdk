﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yepex.Ecommerce.SDK
{           
    public class BaseConstants
    {
        // The Constant SETTING_ECOMMERCE_URL. 
        public const String SettingEcommerceURL = "ecommerceWsUrl";

        // The Constant SETTING_RSA_KEY_BASE64. 
        public const  String SettingRSAKeyType = "ecommerceWsRSAKeyType";

        // The Constant RSA_KEY_MODULUS. 
        public const  String SettingRSAKeyEncrypted = "ecommerceWsRSAKeyEncrypted";

        // The Constant SETTING_RSA_INIT_CODE. 
        public const  String SettingRSAInitCode = "ecommerceWsRSAInitCode";

        public const  String YPSHAKey = "YPKEYINITATOR_^%&^%$%ASDASDKJASKJDLASD=";

        // DotNet SdkVersion for yepex ecommerce-core
        public static string SdkVersion { get { return SDKUtils.GetAssemblyVersionForType(typeof(BaseConstants)); } }
    }
}
