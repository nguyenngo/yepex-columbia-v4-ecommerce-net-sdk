﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;

namespace Yepex.Ecommerce.SDK
{
    public class ServiceContext
    {
        /// <summary>
        /// Dynamic configuration
        /// </summary>
        private Dictionary<string, string> dynamicConfig;

        /// <summary>
        /// SDKVersion instance
        /// </summary>
        private SDKVersion sVersion;

        /// <summary>
        /// Explicit default constructor
        /// </summary>
        public ServiceContext() {
            dynamicConfig = new Dictionary<string, string>();
            if (ConfigurationManager.GetSection("yellowpepper") != null)
            {
                NameValueCollection ypConfigSection = ConfigurationManager.GetSection("yellowpepper")  as NameValueCollection;
                if (ypConfigSection[BaseConstants.SettingEcommerceURL] != null) {
                    dynamicConfig.Add(BaseConstants.SettingEcommerceURL, ypConfigSection[BaseConstants.SettingEcommerceURL]);
                }
                if (ypConfigSection[BaseConstants.SettingRSAKeyType] != null) {
                    dynamicConfig.Add(BaseConstants.SettingRSAKeyType, ypConfigSection[BaseConstants.SettingRSAKeyType]);
                }
                if (ypConfigSection[BaseConstants.SettingRSAKeyEncrypted] != null) {
                    dynamicConfig.Add(BaseConstants.SettingRSAKeyEncrypted, ypConfigSection[BaseConstants.SettingRSAKeyEncrypted]);
                }
                if (ypConfigSection[BaseConstants.SettingRSAInitCode] != null) {
                    dynamicConfig.Add(BaseConstants.SettingRSAInitCode, ypConfigSection[BaseConstants.SettingRSAInitCode]);
                }
            }

        }

        /// <summary>
        /// Gets and sets the Dynamic Configuration
        /// </summary>
        public Dictionary<string, string> Config
        {
            get
            {
                return this.dynamicConfig;
            }
            set
            {
                this.dynamicConfig = value;
            }
        }

        public SDKVersion SdkVersion
        {
            get
            {
                return sVersion;
            }
            set
            {
                sVersion = value;
            }
        }
    }
}