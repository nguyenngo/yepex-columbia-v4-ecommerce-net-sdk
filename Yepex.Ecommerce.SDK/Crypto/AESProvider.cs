﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Yepex.Ecommerce.SDK.Crypto
{
    public class AESProvider
    {
        /// <summary>
        /// The encryption key which will be used for encrypting
        /// </summary>
        public byte[] key { get; set; }

        /// <summary>
        /// The decrypted string
        /// </summary>
        public String decryptedString { get; set; }

        /// <summary>
        /// The encrypted string
        /// </summary>
        public String encryptedString { get; set; }

        /// <summary>
        /// take the key and encrypt it using SHA1 then
        /// return the encrypted data
        /// </summary>
        /// <param name="myKey">key text you will enterd to encrypt it</param>
        public void SetKey(String myKey)
        {
            String combinedKey = myKey + BaseConstants.YPSHAKey;
            //create new instance of md5
            SHA256 sha256 = SHA256.Create();
            //convert the input text to array of bytes
            byte[] hashData = sha256.ComputeHash(Encoding.Default.GetBytes(combinedKey));
            this.key = hashData;
         }

        /// <summary>
        /// take the key and encrypt it using AES then
        /// return the encrypted data
        /// </summary>
        /// <param name="data">key text you will entered to encrypt it</param>
        /// <returns>return the encrypted text as hexadecimal string</returns>
        public String Encrypt(String strToEncrypt)
        {
            byte[] IV = new byte[16];
            Array.Copy(this.key, 0, IV, 0, 16);
            byte[] plainBytes = EncryptStringToBytes_Aes(strToEncrypt, this.key, IV);
            return Convert.ToBase64String(plainBytes, Base64FormattingOptions.None);
        }

        /// <summary>
        /// take the key and decrypt it using AES then
        /// return the decrypted data
        /// </summary>
        /// <param name="data">key text you will entered to decrypt it</param>
        /// <returns>return the plain text string</returns>
        public String Decrypt(String strToDecrypt)
        {
            byte[] IV = new byte[16];
            Array.Copy(this.key, 0, IV, 0, 16);
            byte[] cipherBytes = Convert.FromBase64String(strToDecrypt);
            return DecryptStringFromBytes_Aes(cipherBytes, this.key, IV);
        }

        /// <summary>
        /// Encrypt string via AES Crypto
        /// </summary>
        private byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Create an AesManaged object 
            // with the specified key and IV. 
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return encrypted;
        }

        /// <summary>
        /// Decrypt string via AES Crypto
        /// </summary>
        private string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an AesManaged object 
            // with the specified key and IV. 
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream 
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }
    }
}

