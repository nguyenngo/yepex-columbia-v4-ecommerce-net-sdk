﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Security;

namespace Yepex.Ecommerce.SDK.Crypto
{
    public class RSAProvider
    {
        public String rsaEncryptByBountyCastle(String rsaPublicKey, String plainText) {
            //String RSAKeyBase64 = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAp65TbOICWimhqGAnbGO2o6GEXuiXSwh6qBUxiAtMijk0FpLXV+4zxvR7wZx+nYQWo2j4gmptHKO6/3EkpABf6ghCn6cV1pZFmR5PS2/wIrlCSyhSEt7EM/t5zcKb4pEv3/grXCv4x36aA6rDyUjEHdr2oQvdTXgzFOHabwdxoXHlZhjnNJUZVVvjoRdxfes35aU+0diMr0+3nn/pWbzVKBzpmbz4QbiH8uWTd8kx2ogb+h5zcmpxyQAEBMG9hHRFHUmRH5GSDN0WQ2S7BEd5TTEHGM43ESX0WbxWVmY7I45W6zO7Q5crSsx6DgLTIMTwVyis1Vfp02lNiibTWT+Lm2eJyJkpfMMKKTvd+Q7qCJijpOLy6s+3T2E0fjkgWVsCCPC1arCl9jm1WwzItP45jqLQhlQptQsa7WqboJjJ1w6yvzYVuBtv/M+EO3AaKaTyhw5wlFgh7CWoM+Or55NfKKWIgxCsVQNX1XylaB9l5KTc5lTC7UcivSc5iFRtLQEDWJOXQqlBarXvpHCR6/mzOnzong6g7L5suD3+lWcoEiWGwkNkpva7KyUQK/6N+cvtaBsawlp+nt4yevP8NOxl62UOyHgDtKXY4xCCk2qM3j8h8AYhux06EKtvYTECCyxzQMNvqqqUj0NHO+SH7vLb2uA58s58sToTBcrn4jgA20ECAwEAAQ==";
            var bytes = Encoding.UTF8.GetBytes(plainText);
            byte[] data = Convert.FromBase64String(rsaPublicKey);
            RSAParameters rsaParams = DecodeX509PublicKey(data);
            var rsaKeyParameters = DotNetUtilities.GetRsaPublicKey(rsaParams);
            var cipher = CipherUtilities.GetCipher("RSA/ECB/PKCS1Padding");
            cipher.Init(true, rsaKeyParameters);
            var processBlock = cipher.DoFinal(bytes);
            String base64String = Convert.ToBase64String(processBlock);
            base64String = base64String.Replace(" ", "+");
            //base64String = "HBaWVRtHw5INRevcVg2xjQZJ1foyJYRZV0ZHJbcCrUHbOkwU3cWS2qpyu8T9J/2wh+PMrFVNvdRSCAxwIZdDcU56lWQrnF6nd45XmSEG+IF3uHmck2rsAMoDnn/7G/38mZl/ZyU0rd55LSNqkPeJDnoZKeMaadtEbgaFeQe9wdpwThYjsTTuZNmz3EgPy8IxLiqJOVxq93zXCZfbTp/5GvgtyHHjOzivcNMPq9PwdFptMLeywir+X91lClVlqeNpp1zp6Wdim1PJmv17IE0Ko7e9Z1wMEma6unq8qzPdOuZwN+ADMQFSQtEQ+mJYI1M8JZAhjgqsmS+rr1rDIboS+JH+/N6HPYqIiD9WlbPhjz7UAf7+8zNjBlGIzi6ujLstwgYZfUox+F9S9VqrUS4BebV725DJAd94g13tO3CZpzH/njnwyDKUNFGkmrG4pqIoKKc3Bg6CtUgDFuFgBkZkODc+qkPkXYAfX8nT2ezbBDgcdqpSyP2nE/fH3TX8/cZD5HjtYJscEukuQKKpPYMYX6nebODy/8mVxvWsc48NwOfLbWMhi7H+oA/YMjxatsIoNQqbg1YoEGzlNaJZK+Unhl0nrc8CwKz+8Fmhr2oN7iF+j2SOfyYKc7TVLSV8dhBAXViEFSd6Y5B31CuDRErtGQ/qyvjFWe21e8ZpDdnbiLM=";
            return base64String;
        }

        public String rsaEncryptNormal(String rsaPublicKey, String plainText)
        {
            try
            {
                byte[] data = Convert.FromBase64String(rsaPublicKey);
                RSAParameters rsaParams = DecodeX509PublicKey(data);
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.ImportParameters(rsaParams);
                ASCIIEncoding asciiEncoder = new ASCIIEncoding();
                byte[] encryptedBytes = rsa.Encrypt(asciiEncoder.GetBytes(plainText), true);
                Array.Reverse(encryptedBytes);
                return Convert.ToBase64String(encryptedBytes);
            }
            catch (ArgumentNullException)
            {
                //Catch this exception in case the encryption did 
                //not succeed.
                Console.WriteLine("Encryption failed.");

            }
            return null;
        }

        public String rsaEncrypt(String rsaPublicKeyEncrypted, String rsaInitCode, String plainText)
        {
            try            {
                String rsaPlainKey = decryptRSAKeyWithInitCode(rsaPublicKeyEncrypted, rsaInitCode);
                return rsaEncryptByBountyCastle(rsaPlainKey, plainText);
            }
            catch (ArgumentNullException)
            {
                //Catch this exception in case the encryption did 
                //not succeed.
                Console.WriteLine("Encryption failed.");
            }
            return null;
        }

        protected RSAParameters DecodeX509PublicKey(byte[] x509key)
        {
            byte[] SeqOID = { 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01 };

            MemoryStream ms = new MemoryStream(x509key);
            BinaryReader reader = new BinaryReader(ms);

            if (reader.ReadByte() == 0x30)
                ReadASNLength(reader); //skip the size
            else
                return new RSAParameters();

            int identifierSize = 0; //total length of Object Identifier section
            if (reader.ReadByte() == 0x30)
                identifierSize = ReadASNLength(reader);
            else
                return new RSAParameters();

            if (reader.ReadByte() == 0x06) //is the next element an object identifier?
            {
                int oidLength = ReadASNLength(reader);
                byte[] oidBytes = new byte[oidLength];
                reader.Read(oidBytes, 0, oidBytes.Length);
                if (oidBytes.SequenceEqual(SeqOID) == false) //is the object identifier rsaEncryption PKCS#1?
                    return new RSAParameters();

                int remainingBytes = identifierSize - 2 - oidBytes.Length;
                reader.ReadBytes(remainingBytes);
            }

            if (reader.ReadByte() == 0x03) //is the next element a bit string?
            {
                ReadASNLength(reader); //skip the size
                reader.ReadByte(); //skip unused bits indicator
                if (reader.ReadByte() == 0x30)
                {
                    ReadASNLength(reader); //skip the size
                    if (reader.ReadByte() == 0x02) //is it an integer?
                    {
                        int modulusSize = ReadASNLength(reader);
                        byte[] modulus = new byte[modulusSize];
                        reader.Read(modulus, 0, modulus.Length);
                        if (modulus[0] == 0x00) //strip off the first byte if it's 0
                        {
                            byte[] tempModulus = new byte[modulus.Length - 1];
                            Array.Copy(modulus, 1, tempModulus, 0, modulus.Length - 1);
                            modulus = tempModulus;
                        }

                        if (reader.ReadByte() == 0x02) //is it an integer?
                        {
                            int exponentSize = ReadASNLength(reader);
                            byte[] exponent = new byte[exponentSize];
                            reader.Read(exponent, 0, exponent.Length);
            
                            RSAParameters RSAKeyInfo = new RSAParameters();
                            RSAKeyInfo.Modulus = modulus;
                            RSAKeyInfo.Exponent = exponent;                           
                            return RSAKeyInfo;
                        }
                    }
                }
            }
            return new RSAParameters();
        }

        protected int ReadASNLength(BinaryReader reader)
        {
            //Note: this method only reads lengths up to 4 bytes long as
            //this is satisfactory for the majority of situations.
            int length = reader.ReadByte();
            if ((length & 0x00000080) == 0x00000080) //is the length greater than 1 byte
            {
                int count = length & 0x0000000f;
                byte[] lengthBytes = new byte[4];
                reader.Read(lengthBytes, 4 - count, count);
                Array.Reverse(lengthBytes); //
                length = BitConverter.ToInt32(lengthBytes, 0);
            }
            return length;
        }

        public String encryptRSAKeyWithInitCode(String rsaKeyBase64, String initCode)
        {

            AESProvider aesProvider = new AESProvider();
            aesProvider.SetKey(initCode);
            String cipherRSAKey = aesProvider.Encrypt(rsaKeyBase64);
            return cipherRSAKey;
        }

        public String decryptRSAKeyWithInitCode(String cipherRSAKey, String initCode)
        {

            AESProvider aesProvider = new AESProvider();
            aesProvider.SetKey(initCode);
            String rsaKeyBase64 = aesProvider.Decrypt(cipherRSAKey);
            return rsaKeyBase64;
        }
    }
}
