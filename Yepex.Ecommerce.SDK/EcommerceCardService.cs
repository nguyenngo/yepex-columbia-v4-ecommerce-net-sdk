﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Yepex.Ecommerce.SDK.YepexEcommerceService;
using Yepex.Ecommerce.SDK.Crypto;
using System.ServiceModel;

namespace Yepex.Ecommerce.SDK
{
    public class EcommerceCardService
    {
        /// <summary>
        /// Add new cards on TT data bases, the main function of this method is to enable  third party application
        /// enable to securely save credit card information. Before add card on TT, we need to validate if the card 
        /// information given by user, it’s a valid card.
        /// It will invoke the YP Webservice and send the add card request. The input data is definied in WS Document.
        /// Before send data to payment system. SDK will encrypte the creditcard number because of PCI-DSS requirement.
        /// The used algorithm is RSA, public card is provided by TT Server which controlled by bank and YP is forwarding 
        /// card information only. 
        /// </summary>
        /// <param name="serviceContext">ServiceContext used for the Add Card Service call.</param>
        /// <param name="addCardRequest">the addCard object to be used for add the card into Payment System.</param>
        /// <returns>YepexEcommerceService.respuestaAgregarTarjeta</returns>
        public static YepexEcommerceService.respuestaAgregarTarjeta doAddCardRequest(ServiceContext serviceContext, YepexEcommerceService.solicitudAgregarTarjeta addCardRequest)
        {
            RSAProvider rsaProvider = new RSAProvider();
            String creditCardNumber = addCardRequest.numero_Tarjeta;
            String rsaPublicKeyEncrypted = serviceContext.Config[BaseConstants.SettingRSAKeyEncrypted];
            String rsaPublicInitCode = serviceContext.Config[BaseConstants.SettingRSAInitCode];
            String encryptedCreditCardNumber = rsaProvider.rsaEncrypt(rsaPublicKeyEncrypted, rsaPublicInitCode, creditCardNumber);
            addCardRequest.numero_Tarjeta = encryptedCreditCardNumber;
            ServicioWeb_eCommerceClient client = new ServicioWeb_eCommerceClient();
            client.Endpoint.Address = new EndpointAddress(serviceContext.Config[BaseConstants.SettingEcommerceURL]);
            YepexEcommerceService.respuestaAgregarTarjeta addCardResponse = client.agregarTarjeta(addCardRequest);
            return addCardResponse;
        }

        /// <summary>
        /// Enable the user of those third party apps to get information of the Alias of the cards previously 
        /// registered by their users using the method AddCard (ALIAS = name given by the user to identify one its cards)
        /// </summary>
        /// <param name="serviceContext">ServiceContext used for the Get Card Alias call.</param>
        /// <param name="getCardAliasRequest">the getCard object to be used for get card alias from Payment System.</param>
        /// <returns>YepexEcommerceService.respuestaObtenerAlias</returns>
        public static YepexEcommerceService.respuestaObtenerAlias doGetCardAliasRequest(ServiceContext serviceContext, YepexEcommerceService.solicitudObtenerAlias getCardAliasRequest)
        {
            ServicioWeb_eCommerceClient client = new ServicioWeb_eCommerceClient();
            client.Endpoint.Address = new EndpointAddress(serviceContext.Config[BaseConstants.SettingEcommerceURL]);
            YepexEcommerceService.respuestaObtenerAlias getCardAliasResponse = client.entregarAliasTarjeta(getCardAliasRequest);
            return getCardAliasResponse;
        }

        /// <summary>
        ///  The main function of this method is to allow the commerce remove credit cards on the repository.
        ///  On the TT we have to remove (or change status), if the users of the third parties’ 
        ///  doesn’t want uses the app anymore; so for security need to eliminate the register on the TT database.
        /// </summary>
        /// <param name="serviceContext">ServiceContext used for the Remove Card call.</param>
        /// <param name="removeCardRequest">the remove card object to be used for remove card from Payment System.</param>
        /// <returns>YepexEcommerceService.respuestaEliminarTarjeta</returns>
        public static YepexEcommerceService.respuestaEliminarTarjeta doRemoveCardRequest(ServiceContext serviceContext, YepexEcommerceService.solicitudEliminarTarjeta removeCardRequest)
        {
            ServicioWeb_eCommerceClient client = new ServicioWeb_eCommerceClient();
            client.Endpoint.Address = new EndpointAddress(serviceContext.Config[BaseConstants.SettingEcommerceURL]);
            YepexEcommerceService.respuestaEliminarTarjeta removeCardResponse = client.eliminarTarjeta(removeCardRequest);
            return removeCardResponse;
        }

        /// <summary>
        /// Enables the merchant to request the card information ( card # & expiry date) of a card previously 
        /// registered in the repository (TT) using the method OnCardAddedEnablemerchant
        /// </summary>
        /// <param name="serviceContext">ServiceContext used for the Remove Card call.</param>
        /// <param name="getEncryptedCardRequest">the get encrypted card object to be used for get card from Payment System.</param>
        /// <returns>YepexEcommerceService.respuestaEliminarTarjeta</returns>
        public static YepexEcommerceService.respuestaObtenerTarjetaEncriptada doGetEncryptedCardRequest(ServiceContext serviceContext, YepexEcommerceService.solicitudObtenerTarjetaEncriptada getEncryptedCardRequest)
        {
            ServicioWeb_eCommerceClient client = new ServicioWeb_eCommerceClient();
            client.Endpoint.Address = new EndpointAddress(serviceContext.Config[BaseConstants.SettingEcommerceURL]);
            YepexEcommerceService.respuestaObtenerTarjetaEncriptada getEncryptedCardResponse = client.entregarTarjeta(getEncryptedCardRequest);
            return getEncryptedCardResponse;
        }


    }
}
