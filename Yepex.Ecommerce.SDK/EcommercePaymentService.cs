﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Yepex.Ecommerce.SDK.YepexEcommerceService;
using System.ServiceModel;

namespace Yepex.Ecommerce.SDK
{
    public class EcommercePaymentService
    {
        /// <summary>
        /// The main function of this method is to enable the merchant to implement a payment
        /// using a credit card saved on the TT. The step to get a payment is the following, the
        /// client open his 3rd parties’ app, this app sends to his server the payment request
        /// with the following information (ID of the user, ID of the app, alias, value, merchant
        /// id and other data requested by the authorize payment method), the server of the 3rd
        /// parties’ app sets an API communication with ME server, and the ME with this
        /// information ask to Credibanco eCommerce API to authorize the payment.
        /// The ME server is in charge to get the authorization of the payment through the
        /// eCommerce API of Credibanco, and after that, proceed with the payment on
        /// credibanco servers; when the entire process get finish off, we have to respond back
        /// to the merchant servers, and inform the results of the process.
        /// </summary>
        /// <param name="serviceContext">ServiceContext used for the Add Card Service call.</param>
        /// <param name="paymentRequest">the payment request object to be used for paymnet.</param>
        /// <returns>YepexEcommerceService.respuestaAgregarTarjeta</returns>
        public static YepexEcommerceService.respuestaSolicitudPago doPaymentRequest(ServiceContext serviceContext, YepexEcommerceService.solicitudPago paymentRequest)
        {
            ServicioWeb_eCommerceClient client = new ServicioWeb_eCommerceClient();
            client.Endpoint.Address = new EndpointAddress(serviceContext.Config[BaseConstants.SettingEcommerceURL]);
            YepexEcommerceService.respuestaSolicitudPago paymentResponse = client.pagarConTarjeta(paymentRequest);
            return paymentResponse;
        }


    }
}
