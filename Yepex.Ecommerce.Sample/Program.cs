﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Yepex.Ecommerce.SDK.Crypto;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.ComponentModel;
namespace Yepex.Ecommerce.Sample
{
    class Program
    {
        static void Main(string[] args)
        {
            String publicKey = "MIIGdzCCBV+gAwIBAgIKEpMtNAAIAAKN5TANBgkqhkiG9w0BAQUFADCBizETMBEGCgmSJomT8ixkARkWA2NvbTEZMBcGCgmSJomT8ixkARkWCW1pY3Jvc29mdDEUMBIGCgmSJomT8ixkARkWBGNvcnAxFzAVBgoJkiaJk/IsZAEZFgdyZWRtb25kMSowKAYDVQQDEyFNaWNyb3NvZnQgU2VjdXJlIFNlcnZlciBBdXRob3JpdHkwHhcNMTIwNjA1MTY1OTMzWhcNMTQwNTE5MjIyMzMwWjB1MQswCQYDVQQGEwJVUzELMAkGA1UECBMCV0ExEDAOBgNVBAcTB1JlZG1vbmQxEjAQBgNVBAoTCU1pY3Jvc29mdDESMBAGA1UECxMJWGJveCBMaXZlMR8wHQYDVQQDExZ4cGFzc3BvcnQueGJveGxpdmUuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvUihVNnWYpu3uJmcLy+PBecKu4ziVD7OIeZ/V+tJkXbc5+6OW8G+QDtJKuJkkuxGNLBNmLHbCyXsJ/US3kKkU7/7yK7jfWRNdqAKJdDTVxsWnxlo+/28ScGrAV6wK2bbK8GQBpsYRn1HKGCGceWIBCSqUfI7rwgwDnvqcW5PeivORd4+or5DdhgUMwiV5Vr2fvdcAiQR1CKgMphxO4+OmZ4khpB/HT/xS4FscvfFsSBLM37jBMrnhY5yNKPeHZB2eYvehnnw22NFHJNksa+vVFXL9aJcZWJc/bqqlhlhL8eLdYSR/KA006PSInW8yWtd4IFVKJ1Moa41gCUZL81voQIDAQABo4IC8DCCAuwwRAYJKoZIhvcNAQkPBDcwNTAOBggqhkiG9w0DAgICAIAwDgYIKoZIhvcNAwQCAgCAMAcGBSsOAwIHMAoGCCqGSIb3DQMHMB0GA1UdDgQWBBS9zuJeIfYW3Q6Jd9KcoLuNM8CNcTAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwCwYDVR0PBAQDAgSwMB8GA1UdIwQYMBaAFAhC49tOEWbztQjFQNtVfDNGEYM4MIIBCgYDVR0fBIIBATCB/jCB+6CB+KCB9YZYaHR0cDovL21zY3JsLm1pY3Jvc29mdC5jb20vcGtpL21zY29ycC9jcmwvTWljcm9zb2Z0JTIwU2VjdXJlJTIwU2VydmVyJTIwQXV0aG9yaXR5KDgpLmNybIZWaHR0cDovL2NybC5taWNyb3NvZnQuY29tL3BraS9tc2NvcnAvY3JsL01pY3Jvc29mdCUyMFNlY3VyZSUyMFNlcnZlciUyMEF1dGhvcml0eSg4KS5jcmyGQWh0dHA6Ly9jb3JwcGtpL2NybC9NaWNyb3NvZnQlMjBTZWN1cmUlMjBTZXJ2ZXIlMjBBdXRob3JpdHkoOCkuY3JsMIG/BggrBgEFBQcBAQSBsjCBrzBeBggrBgEFBQcwAoZSaHR0cDovL3d3dy5taWNyb3NvZnQuY29tL3BraS9tc2NvcnAvTWljcm9zb2Z0JTIwU2VjdXJlJTIwU2VydmVyJTIwQXV0aG9yaXR5KDgpLmNydDBNBggrBgEFBQcwAoZBaHR0cDovL2NvcnBwa2kvYWlhL01pY3Jvc29mdCUyMFNlY3VyZSUyMFNlcnZlciUyMEF1dGhvcml0eSg4KS5jcnQwPwYJKwYBBAGCNxUHBDIwMAYoKwYBBAGCNxUIg8+JTa3yAoWhnwyC+sp9geH7dIFPg8LthQiOqdKFYwIBZAIBCjAnBgkrBgEEAYI3FQoEGjAYMAoGCCsGAQUFBwMCMAoGCCsGAQUFBwMBMA0GCSqGSIb3DQEBBQUAA4IBAQBhJKcKn7h3/pZK2A5wk6lonJMyra4u3bFLzfvg0HWWiGx+vWCbBA2EJoar8V1yCMKezAQOcJWDKzO5HzY9m+PGm7FZtoYZWmCaYt2+Hlt/X7py3Nhgey/xYBTXb1rnu9jhk84+lG4dSAyyTYrsCgrCG5vsqGM4ZZz7FGOhUWQ4QAZ36vgSoLpOy6/6xpaWor4ritklCmHYYGpyuUuZLBt/Tu44ng2rh98hyxMNgaBfZ1cpqnMyYatWIgPPg8DEuNF4iGaujIFQfrU2VuyiUvFWLA9H5vfVLh5CBYR7qqRda0g4p22FDxdfmLu4Q2iZg+rbGxu9+g/9AdAXvhMEjvqC";
            String publicKey2 = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAp65TbOICWimhqGAnbGO2o6GEXuiXSwh6qBUxiAtMijk0FpLXV+4zxvR7wZx+nYQWo2j4gmptHKO6/3EkpABf6ghCn6cV1pZFmR5PS2/wIrlCSyhSEt7EM/t5zcKb4pEv3/grXCv4x36aA6rDyUjEHdr2oQvdTXgzFOHabwdxoXHlZhjnNJUZVVvjoRdxfes35aU+0diMr0+3nn/pWbzVKBzpmbz4QbiH8uWTd8kx2ogb+h5zcmpxyQAEBMG9hHRFHUmRH5GSDN0WQ2S7BEd5TTEHGM43ESX0WbxWVmY7I45W6zO7Q5crSsx6DgLTIMTwVyis1Vfp02lNiibTWT+Lm2eJyJkpfMMKKTvd+Q7qCJijpOLy6s+3T2E0fjkgWVsCCPC1arCl9jm1WwzItP45jqLQhlQptQsa7WqboJjJ1w6yvzYVuBtv/M+EO3AaKaTyhw5wlFgh7CWoM+Or55NfKKWIgxCsVQNX1XylaB9l5KTc5lTC7UcivSc5iFRtLQEDWJOXQqlBarXvpHCR6/mzOnzong6g7L5suD3+lWcoEiWGwkNkpva7KyUQK/6N+cvtaBsawlp+nt4yevP8NOxl62UOyHgDtKXY4xCCk2qM3j8h8AYhux06EKtvYTECCyxzQMNvqqqUj0NHO+SH7vLb2uA58s58sToTBcrn4jgA20ECAwEAAQ==";
            String plainText = "This is plaintext";
            String cipherText = "CAvyfO/+YJDgyfsfB5vZJIs2hKPgdk2RzmKVc2Mm9jlb1+K02ebenjYm5IniC3TljDXc1M6bWAuQol7ovPqObLrRzki3bw3nfRwAb8gU8+A9t88ztr9LHkYRRyKz92jzUZ/KlSdaZl8ei9lHyNwroGWwNorr7Qg507nQeT3hQ3qsSBe1bZG1KZTQjRati+M5SV9sxDJ/7spGGDK7irqIch/zFXcIj84ja1fQFAoFCMWxAYI5nBhks3SVshq/kBJAY2RhEwnqv38NtQRqVJrWHemxVith8lJjwEzmZxIS7LgeTI/AZ2ooRjiCKMq31AXzV3cFkA7SFk8pkgUPZp42oZa4YwjO4aq/HAgCZ35kTUUu5SfFUagrbD9GXaeXsctx18zBypS8PQBXcUGwFtWja+wA/a3aJeN5iXqrJhUQrscYW6He6xx0AdBnsmhA8w8pVtdKu8osYxQBwNgUfE8iEZJOA88ahMUzcbtyGjdsgMLdIa004l7BITaQ4bwR8D3zwOb1yK3Zd3S02+Tm5B9Q7/Q3xRCR3A7O3Xan9VNSmZJNx40oaP81XaHBNDzRjmaiefFf9GC7jKG7qrTdWIYLsBgVuZLHtfmWAVDFL1agss9kT066qEjiLhJSsu9d5Hb4SXHhrzWCGbOzpD+FxlSBapIUgSW0LJkHxxQ2TJwvufg=";
            byte[] data = Convert.FromBase64String(publicKey);

            X509Certificate2 x509certificate = new X509Certificate2(data);
            byte[] rsaPublicKey = x509certificate.GetPublicKey();

            String base64 = Convert.ToBase64String(rsaPublicKey);

            Console.WriteLine(base64);
            Console.WriteLine(base64.Length);
            Console.WriteLine(publicKey.Length);
            Console.WriteLine(publicKey2.Length);

            //RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)x509certificate.GetPublicKey();
            RSACryptoServiceProvider rsa = DecodeX509PublicKey(Convert.FromBase64String(publicKey2));
            String testvalue = "Encrypt text";
            ASCIIEncoding asciiEncoder = new ASCIIEncoding();
            byte[] encryptString = rsa.Encrypt(asciiEncoder.GetBytes(plainText), false);
            Console.WriteLine(Convert.ToBase64String(encryptString));
            Console.WriteLine(cipherText);
            Console.ReadKey();
        }

        public static RSACryptoServiceProvider DecodeX509PublicKey(byte[] x509key)
        {
            byte[] SeqOID = { 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01 };

            MemoryStream ms = new MemoryStream(x509key);
            BinaryReader reader = new BinaryReader(ms);

            if (reader.ReadByte() == 0x30)
                ReadASNLength(reader); //skip the size
            else
                return null;

            int identifierSize = 0; //total length of Object Identifier section
            if (reader.ReadByte() == 0x30)
                identifierSize = ReadASNLength(reader);
            else
                return null;

            if (reader.ReadByte() == 0x06) //is the next element an object identifier?
            {
                int oidLength = ReadASNLength(reader);
                byte[] oidBytes = new byte[oidLength];
                reader.Read(oidBytes, 0, oidBytes.Length);
                if (oidBytes.SequenceEqual(SeqOID) == false) //is the object identifier rsaEncryption PKCS#1?
                    return null;

                int remainingBytes = identifierSize - 2 - oidBytes.Length;
                reader.ReadBytes(remainingBytes);
            }

            if (reader.ReadByte() == 0x03) //is the next element a bit string?
            {
                ReadASNLength(reader); //skip the size
                reader.ReadByte(); //skip unused bits indicator
                if (reader.ReadByte() == 0x30)
                {
                    ReadASNLength(reader); //skip the size
                    if (reader.ReadByte() == 0x02) //is it an integer?
                    {
                        int modulusSize = ReadASNLength(reader);
                        byte[] modulus = new byte[modulusSize];
                        reader.Read(modulus, 0, modulus.Length);
                        if (modulus[0] == 0x00) //strip off the first byte if it's 0
                        {
                            byte[] tempModulus = new byte[modulus.Length - 1];
                            Array.Copy(modulus, 1, tempModulus, 0, modulus.Length - 1);
                            modulus = tempModulus;
                        }

                        if (reader.ReadByte() == 0x02) //is it an integer?
                        {
                            int exponentSize = ReadASNLength(reader);
                            byte[] exponent = new byte[exponentSize];
                            reader.Read(exponent, 0, exponent.Length);

                            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
                            RSAParameters RSAKeyInfo = new RSAParameters();
                            RSAKeyInfo.Modulus = modulus;
                            RSAKeyInfo.Exponent = exponent;
                            RSA.ImportParameters(RSAKeyInfo);
                            return RSA;
                        }
                    }
                }
            }
            return null;
        }

        public static int ReadASNLength(BinaryReader reader)
        {
            //Note: this method only reads lengths up to 4 bytes long as
            //this is satisfactory for the majority of situations.
            int length = reader.ReadByte();
            if ((length & 0x00000080) == 0x00000080) //is the length greater than 1 byte
            {
                int count = length & 0x0000000f;
                byte[] lengthBytes = new byte[4];
                reader.Read(lengthBytes, 4 - count, count);
                Array.Reverse(lengthBytes); //
                length = BitConverter.ToInt32(lengthBytes, 0);
            }
            return length;
        }
    }
}
