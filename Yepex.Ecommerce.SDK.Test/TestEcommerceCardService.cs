﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Yepex.Ecommerce.SDK.Test
{
    [TestClass]
    public class TestEcommerceCardService
    {

        [TestMethod]
        public void testAddCardService()
        {    
            ServiceContext serviceContext = TestingUtils.GetServiceContext();
            YepexEcommerceService.solicitudAgregarTarjeta addCardRequest = EcommerceCardData.createSampleAddCardRequest();                
            YepexEcommerceService.respuestaAgregarTarjeta addCardResponse = EcommerceCardService.doAddCardRequest(serviceContext, addCardRequest);
            Assert.IsNotNull(addCardResponse);
            Assert.AreEqual(addCardRequest.id_Transaccion, addCardResponse.id_Transaccion);
        }

        [TestMethod]
        public void testGetCardAliasService()
        {
            ServiceContext serviceContext = TestingUtils.GetServiceContext();
            YepexEcommerceService.solicitudObtenerAlias getCardAliasRequest = EcommerceCardData.createSampleGetCardAliasRequest();
            YepexEcommerceService.respuestaObtenerAlias getCardAliasResponse = EcommerceCardService.doGetCardAliasRequest(serviceContext, getCardAliasRequest);
            Assert.IsNotNull(getCardAliasResponse);
            Assert.AreEqual(getCardAliasRequest.id_Transaccion, getCardAliasResponse.id_Transaccion);
        }

        [TestMethod]
        public void testRemoveCardService()
        {
            ServiceContext serviceContext = TestingUtils.GetServiceContext();
            YepexEcommerceService.solicitudEliminarTarjeta removeCardRequest = EcommerceCardData.createSampleRemoveCardRequest();
            YepexEcommerceService.respuestaEliminarTarjeta removeCardResponse = EcommerceCardService.doRemoveCardRequest(serviceContext, removeCardRequest);
            Assert.IsNotNull(removeCardResponse);
            Assert.AreEqual(removeCardRequest.id_Transaccion, removeCardResponse.id_Transaccion);
        }

        [TestMethod]
        public void testGetCardService()
        {
            ServiceContext serviceContext = TestingUtils.GetServiceContext();
            YepexEcommerceService.solicitudAgregarTarjeta getCardRequest = EcommerceCardData.createSampleAddCardRequest();
            YepexEcommerceService.respuestaAgregarTarjeta getCardResponse = EcommerceCardService.doAddCardRequest(serviceContext, getCardRequest);
            Assert.IsNotNull(getCardResponse);
            Assert.AreEqual(getCardResponse.id_Transaccion, getCardRequest.id_Transaccion);
        }
    }
}
