﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yepex.Ecommerce.SDK.Test
{
    public class EcommerceCardData
    {
        /** The Constant ID_TRANSACTION. */
        protected const String ID_TRANSACTION = "1234567890";

        /** The Constant ID_APP. */
        protected const String ID_APP = "1";

        /** The Constant CREDIT_CARD_NUMBER. */
        protected const String CREDIT_CARD_NUMBER = "5546250000039992";

        /** The Constant CREDIT_CARD_CVV. */
        protected const String CREDIT_CARD_CVV = "932";

        /** The Constant CREDIT_CARD_EXPIRATION_DATE. */
        protected const String CREDIT_CARD_EXPIRATION_DATE = "2017-05";

        /** The Constant CARDHOLDER_NAME. */
        protected const String CARDHOLDER_NAME = "testNombreTarjetahabiente";

        /** The Constant CARDHOLDER_ID. */
        protected const String CARDHOLDER_ID = "testIdentificacionTarjetahabiente";

        /** The Constant CARD_ALIAS. */
        protected const String CARD_ALIAS = "testAliasTarjeta";

        /** The Constant ADDRESS. */
        protected const String ADDRESS = "This is fake data test";

        /** The Constant CELLPHONE_NUMBER. */
        protected const String CELLPHONE_NUMBER = "123456789";

        /**
         * Build the sample add card request from mock data.
         *
         * @return the object AddCardRequest
         */
        public static YepexEcommerceService.solicitudAgregarTarjeta createSampleAddCardRequest()
        {
            // IMPORTANT: Below information are created for testing ONLY
            // build the sample add card request from generate file
            YepexEcommerceService.solicitudAgregarTarjeta addCardRequest = new YepexEcommerceService.solicitudAgregarTarjeta();
            addCardRequest.id_Transaccion = ID_TRANSACTION;
            addCardRequest.id_App = ID_APP;
            addCardRequest.numero_Tarjeta = CREDIT_CARD_NUMBER;
            addCardRequest.cvv = CREDIT_CARD_CVV;
            addCardRequest.fecha_Expiracion = CREDIT_CARD_EXPIRATION_DATE;
            addCardRequest.nombre_Tarjetahabiente = CARDHOLDER_NAME;
            addCardRequest.identificacion_Tarjetahabiente = CARDHOLDER_ID;
            addCardRequest.alias_Tarjeta = CARD_ALIAS;
            addCardRequest.dirreccion_Correspondencia = ADDRESS;
            addCardRequest.numero_Celular = CELLPHONE_NUMBER;
            return addCardRequest;
        }

        /**
         * Build the sample remove card request from mock data.
         *
         * @return the object ECommerceRemoveCardRequest
         */
        public static YepexEcommerceService.solicitudEliminarTarjeta createSampleRemoveCardRequest()
        {
            // IMPORTANT: Below information are created for testing ONLY
            // build the remove add card request from generate file
            YepexEcommerceService.solicitudEliminarTarjeta eCommerceRemoveCardRequest = new YepexEcommerceService.solicitudEliminarTarjeta();
            eCommerceRemoveCardRequest.alias_Tarjeta=CARD_ALIAS;
            eCommerceRemoveCardRequest.id_App=ID_APP;
            eCommerceRemoveCardRequest.identificacion_Tarjetahabiente=CARDHOLDER_ID;
            eCommerceRemoveCardRequest.id_Transaccion=ID_TRANSACTION;
            eCommerceRemoveCardRequest.numero_Celular=CELLPHONE_NUMBER;
            return eCommerceRemoveCardRequest;
        }

        /**
         * Build the sample get card alias request from mock data.
         *
         * @return the object ECommerceGetCardAliasRequest
         */
        public static YepexEcommerceService.solicitudObtenerAlias createSampleGetCardAliasRequest()
        {
            // IMPORTANT: Below information are created for testing ONLY
            // build the remove add card request from generate file
            YepexEcommerceService.solicitudObtenerAlias eCommerceGetCardAliasRequest = new YepexEcommerceService.solicitudObtenerAlias();
            eCommerceGetCardAliasRequest.id_App = ID_APP;
            eCommerceGetCardAliasRequest.identificacion_Tarjetahabiente = CARDHOLDER_ID;
            eCommerceGetCardAliasRequest.id_Transaccion = ID_TRANSACTION;
            return eCommerceGetCardAliasRequest;
        }

        /**
         * Build the sample get card alias request from mock data.
         *
         * @return the object ECommerceGetEncryptedCardRequest
         */
        public static YepexEcommerceService.solicitudObtenerTarjetaEncriptada createSampleGetEncryptedCardRequest()
        {
            // IMPORTANT: Below information are created for testing ONLY
            // build the get encrypted card request from generate file
            YepexEcommerceService.solicitudObtenerTarjetaEncriptada eCommerceGetEncryptedCardRequest = new YepexEcommerceService.solicitudObtenerTarjetaEncriptada();
            eCommerceGetEncryptedCardRequest.alias_Tarjeta = CARD_ALIAS;
            eCommerceGetEncryptedCardRequest.id_App = ID_APP;
            eCommerceGetEncryptedCardRequest.identificacion_Tarjetahabiente = CARDHOLDER_ID;
            eCommerceGetEncryptedCardRequest.id_Transaccion = ID_TRANSACTION;
            return eCommerceGetEncryptedCardRequest;
        }
    }
}
