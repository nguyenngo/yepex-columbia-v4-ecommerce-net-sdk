﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Yepex.Ecommerce.SDK.Test
{
    [TestClass]
    public class TestEcommercePaymentService
    {

        [TestMethod]
        public void testPaymentService()
        {    
            ServiceContext serviceContext = TestingUtils.GetServiceContext();
            YepexEcommerceService.solicitudPago paymentRequest = EcommercePaymentData.createSamplePaymentRequest();
            YepexEcommerceService.respuestaSolicitudPago paymentResponse = EcommercePaymentService.doPaymentRequest(serviceContext, paymentRequest);
            Assert.IsNotNull(paymentResponse);
            Assert.AreEqual(paymentResponse.id_Transaccion, paymentRequest.id_Transaccion);
        }

       
    }
}
