﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yepex.Ecommerce.SDK.Test
{
    public class EcommercePaymentData
    {
        /** The Constant ID_APP. */
        protected const String ID_APP = "1";

        /** The Constant ID_TRANSACTION. */
        protected const String ID_TRANSACTION = "1234567890";

        /** The Constant CARD_ALIAS. */
        protected const String CARD_ALIAS = "testAliasTarjeta";

        /** The Constant DESCRIPTION. */
        protected const String DESCRIPTION = "This is fake data test";

        /** The Constant PURCHASE_VALUE. */
        protected const String PURCHASE_VALUE = "100";

        /** The Constant TIP_VALUE. */
        protected const int TIP_VALUE = 10;

        /** The Constant PAYMENT_REFERENCE. */
        protected const String PAYMENT_REFERENCE = "This is fake data test";

        /**
         * Creates the sample payment request.
         *
         * @return the e commerce payment request
         */
        public static YepexEcommerceService.solicitudPago createSamplePaymentRequest()
        {
            // IMPORTANT: Below information are created for testing ONLY
            // build the sample payment request from generate file
            YepexEcommerceService.solicitudPago eCommercePaymentRequest = new YepexEcommerceService.solicitudPago();
            eCommercePaymentRequest.id_App=ID_APP;
            eCommercePaymentRequest.alias_Tarjeta=CARD_ALIAS;
            eCommercePaymentRequest.descripcion_Pago=DESCRIPTION;
            eCommercePaymentRequest.id_Transaccion=ID_TRANSACTION;
            eCommercePaymentRequest.referencia_Pago=PAYMENT_REFERENCE;
            eCommercePaymentRequest.valor_Compra=PURCHASE_VALUE;
            
           
            return eCommercePaymentRequest;
        }
    }
}
