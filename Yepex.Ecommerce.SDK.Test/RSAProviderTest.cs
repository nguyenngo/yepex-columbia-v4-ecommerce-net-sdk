﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Yepex.Ecommerce.SDK.Crypto;

namespace Yepex.Ecommerce.SDK.Test
{
    [TestClass]
    public class RSAProviderTest
    {
        public static readonly String RSAKeyBase64 = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAp65TbOICWimhqGAnbGO2o6GEXuiXSwh6qBUxiAtMijk0FpLXV+4zxvR7wZx+nYQWo2j4gmptHKO6/3EkpABf6ghCn6cV1pZFmR5PS2/wIrlCSyhSEt7EM/t5zcKb4pEv3/grXCv4x36aA6rDyUjEHdr2oQvdTXgzFOHabwdxoXHlZhjnNJUZVVvjoRdxfes35aU+0diMr0+3nn/pWbzVKBzpmbz4QbiH8uWTd8kx2ogb+h5zcmpxyQAEBMG9hHRFHUmRH5GSDN0WQ2S7BEd5TTEHGM43ESX0WbxWVmY7I45W6zO7Q5crSsx6DgLTIMTwVyis1Vfp02lNiibTWT+Lm2eJyJkpfMMKKTvd+Q7qCJijpOLy6s+3T2E0fjkgWVsCCPC1arCl9jm1WwzItP45jqLQhlQptQsa7WqboJjJ1w6yvzYVuBtv/M+EO3AaKaTyhw5wlFgh7CWoM+Or55NfKKWIgxCsVQNX1XylaB9l5KTc5lTC7UcivSc5iFRtLQEDWJOXQqlBarXvpHCR6/mzOnzong6g7L5suD3+lWcoEiWGwkNkpva7KyUQK/6N+cvtaBsawlp+nt4yevP8NOxl62UOyHgDtKXY4xCCk2qM3j8h8AYhux06EKtvYTECCyxzQMNvqqqUj0NHO+SH7vLb2uA58s58sToTBcrn4jgA20ECAwEAAQ==";

        public static readonly String CreditCardSample = "5546250000039992";

        public static readonly String initCode = "AOCAg8AMIICCgKCAgEAp65Tb";
        
        [TestMethod]
        public void testRSAEncrypt()
        {
            RSAProvider provider = new RSAProvider();
            String cipherText = provider.rsaEncryptByBountyCastle(RSAKeyBase64, CreditCardSample);
            Assert.IsNotNull(cipherText);
        }

        [TestMethod]
        public void testRSAkeyEncrypt()
        {
            RSAProvider provider = new RSAProvider();
            String cipherRSAKey = provider.encryptRSAKeyWithInitCode(RSAKeyBase64, initCode);
            Assert.IsNotNull(cipherRSAKey);
            //Assert.Assert.AreEqual(cipherRSAKey, RSAKeyEncrypted);
            String plainRSAKey = provider.decryptRSAKeyWithInitCode(cipherRSAKey, initCode);
            Assert.AreEqual(plainRSAKey, RSAKeyBase64);
        }
    }
}
