﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yepex.Ecommerce.SDK.Test
{
    public class TestingUtils
    {
        public static Dictionary<string, string> GetConfig()
        {
            var config = new Dictionary<string, string>();
            config[BaseConstants.SettingEcommerceURL] = "https://sbstg01-v4.yellowpepper.com/co/yw4/eCommerceWebService?wsdl";
            config[BaseConstants.SettingRSAKeyType] = "AES_ENCRYPTED";
            config[BaseConstants.SettingRSAKeyEncrypted] = "3UOFt5yP9708g/suf3wiSHhI1CeRwzjJvGKaCvy8oMf5NZX08x7dAnLM1cmU1le7iMZyXceNIzZriqstdMhQutvwkkthekfYPkZmaRyLDAKvtytz9dOZNeayrP9iR7CHdRneTDONupqI2kJtNL569T2z32DT3Yl1KPET20yApAF3FJR8f5n03sZDJcE+MY3qnZVdNZNjt61GUkvwm65npErGH83UEraI6p19liXOxa7CNUE1WObvWhCZzWZvy6KeBCeAjCWPWmjVkqZ5UTc/0oKyjInQgXigGqAfSPNoqAm0WG5sxqijK0g7Y3Uz6J68fIPae2HJi4uhslCVtDmLTFdW4TqQHrXdcqMpyw83RpUMtD0VtgW7LkhoWdwJ5uATk86flOk8pvlqWbuDqt0F7YmXkMqNkmJiH/JnKCbkJNAD7u8/M2DWX7IUGyw3bVR82XsraQu/UwkXuxKcQnk3zBj2KhYwMeKKYyMnXfjKfKrHshXSUD9jAFB7pNw3z42H3jnKwWpypiTmgmOhOQjpzlsTaXMQ1dJaGj3ZY7AZiHbMH7oNyeTnAtE0f7En5xl8U6crU8vapU25QMemEafDHwjKQ5g7Ds3856yaSe0wpaJ3rC4YOpuGieA2Jkx4T60Z+G0ME9bniriGAIohyBBI4ISSiwQjJvt7YreHzWfwsQXYWAryCUYIAXvmqOIAQpqCCJiGxRSvTQe03frcVNtmsMAoIiLdYgl06OKZOlUUG+18/JS/tTEFbQ/jbX/CcQxN4lCfGEKpDYO0hEROBmk6odohX+qUdO78/gb5fY7S1zT7IUSipPV5cBQ/eSnR2b5e7fqA0dygRPuDHpALZOqKQBff2+E8IKgEeiOet/mlSzT0SFYkgnl7IvmmcjGxseq0r+Vh+sOzpp7d6Igb/XPePHL/BSi+BmiJBGJ4So4S1S8RhIekB+tazzqRl+IDG4nFMiXBMcGApksdK6leOthpqMOqazBr3UWb8/5WjjlF+SI=";
            config[BaseConstants.SettingRSAInitCode] = "AOCAg8AMIICCgKCAgEAp65Tb";
            return config;
        }

        public static Yepex.Ecommerce.SDK.ServiceContext GetServiceContext ()
        {
            var serviceContext = new Yepex.Ecommerce.SDK.ServiceContext();
            //serviceContext.Config = GetConfig();
            return serviceContext;
        }
    }
}
